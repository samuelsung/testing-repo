{ ... }:

{
  imports = [
    ./boot.nix
    ./file-system.nix
    ./graphics.nix
  ];
}
