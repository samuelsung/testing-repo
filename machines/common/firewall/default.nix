{ allowedTCPPorts ? [ ]
, allowedTCPPortRanges ? [ ]
, allowedUDPPorts ? [ ]
, allowedUDPPortRanges ? [ ]
}: { ... }:

{
  # firewall
  networking.firewall = {
    enable = true;

    allowPing = true;
    # pingLimit
    extraCommands = ''
    '';
    # Description: Additional shell commands executed as part of the firewall initialisation script. These are executed just before the final "reject" firewall rule is added, so they can be used to allow packets that would otherwise be refused. 
    # Default value: ""
    # Type: strings concatenated with "\n"
    # Example value: "iptables -A INPUT -p icmp -j ACCEPT"
    # Declared in: nixos/modules/services/networking/firewall.nix

    extraStopCommands = ''
    '';
    # Description: Additional shell commands executed as part of the firewall shutdown script. These are executed just after the removal of the NixOS input rule, or if the service enters a failed state. 
    # Default value: ""
    # Type: strings concatenated with "\n"
    # Example value: "iptables -P INPUT ACCEPT"
    # Declared in: nixos/modules/services/networking/firewall.nix

    extraPackages = [ ];
    # Description: Additional packages to be included in the environment of the system as well as the path of networking.firewall.extraCommands. 
    # Default value: []
    # Type: list of packages

    # Example value: "[ pkgs.ipset ]"
    # Declared in: nixos/modules/services/networking/firewall.nix

    rejectPackets = false;
    # Description: If set, refused packets are rejected rather than dropped (ignored). This means that an ICMP "port unreachable" error message is sent back to the client (or a TCP RST packet in case of an existing connection). Rejecting packets makes port scanning somewhat easier. 
    # Default value: false
    # Type: boolean
    # Example value: Not given
    # Declared in: nixos/modules/services/networking/firewall.nix

    inherit allowedTCPPorts;
    # Description: List of TCP ports on which incoming connections are accepted. 
    # Default value: []
    # Type: list of 16 bit unsigned integer; between 0 and 65535 (both inclusive)s
    # Example value: [22, 80]
    # Declared in: nixos/modules/services/networking/firewall.nix

    inherit allowedTCPPortRanges;
    # Description: A range of TCP ports on which incoming connections are accepted. 
    # Default value: []
    # Type: list of attribute set of 16 bit unsigned integer; between 0 and 65535 (both inclusive)ss
    # Example value: [{"from": 8999, "to": 9003}]
    # Declared in: nixos/modules/services/networking/firewall.nix

    inherit allowedUDPPorts;
    # Description: List of open UDP ports. 
    # Default value: []
    # Type: list of 16 bit unsigned integer; between 0 and 65535 (both inclusive)s
    # Example value: [53]
    # Declared in: nixos/modules/services/networking/firewall.nix

    inherit allowedUDPPortRanges;
    # Description: Range of open UDP ports. 
    # Default value: []
    # Type: list of attribute set of 16 bit unsigned integer; between 0 and 65535 (both inclusive)ss
    # Example value: [{"from": 60000, "to": 61000}]
    # Declared in: nixos/modules/services/networking/firewall.nix

    checkReversePath = true;
    # Description: Performs a reverse path filter test on a packet. If a reply to the packet would not be sent via the same interface that the packet arrived on, it is refused. If using asymmetric routing or other complicated routing, set this option to loose mode or disable it and setup your own counter-measures. This option can be either true (or "strict"), "loose" (only drop the packet if the source address is not reachable via any interface) or false. Defaults to the value of kernelHasRPFilter. (needs kernel 3.3+) 
    # Default value: true
    # Type: boolean or one of "strict", "loose"
    # Example value: "loose"
    # Declared in: nixos/modules/services/networking/firewall.nix

    logRefusedPackets = false;
    # Description: Whether to log all rejected or dropped incoming packets. This tends to give a lot of log messages, so it's mostly useful for debugging. 
    # Default value: false
    # Type: boolean
    # Example value: Not given
    # Declared in: nixos/modules/services/networking/firewall.nix

    logRefusedConnections = true;
    # Description: Whether to log rejected or dropped incoming connections. 
    # Default value: true
    # Type: boolean
    # Example value: Not given
    # Declared in: nixos/modules/services/networking/firewall.nix

    logRefusedUnicastsOnly = true;
    # Description: If networking.firewall.logRefusedPackets and this option are enabled, then only log packets specifically directed at this machine, i.e., not broadcasts or multicasts.
    # Default value: true
    # Type: boolean
    # Example value: Not given
    # Declared in: nixos/modules/services/networking/firewall.nix

    logReversePathDrops = false;
    # Description: Logs dropped packets failing the reverse path filter test if the option networking.firewall.checkReversePath is enabled. 
    # Default value: false
    # Type: boolean
    # Example value: Not given
    # Declared in: nixos/modules/services/networking/firewall.nix

    trustedInterfaces = [ ];
    # Description: Traffic coming in from these interfaces will be accepted unconditionally. Traffic from the loopback (lo) interface will always be accepted. 
    # Default value: []
    # Type: list of strings
    # Example value: ["enp0s2"]
    # Declared in: nixos/modules/services/networking/firewall.nix

    autoLoadConntrackHelpers = false;
    # Description: Whether to auto-load connection-tracking helpers. See the description at networking.firewall.connectionTrackingModules (needs kernel 3.5+) 
    # Default value: false
    # Type: boolean
    # Example value: Not given
    # Declared in: nixos/modules/services/networking/firewall.nix

    connectionTrackingModules = [ ];
    # Description: List of connection-tracking helpers that are auto-loaded. The complete list of possible values is given in the example. As helpers can pose as a security risk, it is advised to set this to an empty list and disable the setting networking.firewall.autoLoadConntrackHelpers unless you know what you are doing. Connection tracking is disabled by default. Loading of helpers is recommended to be done through the CT target. More info: https://home.regit.org/netfilter-en/secure-use-of-helpers/ 
    # Default value: []
    # Type: list of strings
    # Example value: ["ftp", "irc", "sane", "sip", "tftp", "amanda", "h323", "netbios_sn", "pptp", "snmp"]
  };
}
