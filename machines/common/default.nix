{
  bluetooth = import ./bluetooth;
  brightness-control = import ./brightness-control;
  firewall = import ./firewall;
  networkmanager = import ./networkmanager;
  ntp = import ./ntp;
  opengl-intel = import ./opengl/intel.nix;
  samba = import ./samba;
  sound = import ./sound;
  ssh = import ./ssh;
  touchpad = import ./touchpad;
  wireguard = import ./wireguard;
}
