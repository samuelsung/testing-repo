{ pkgs, ... }:

{
  # networkmanager
  networking.networkmanager = {
    enable = true;

    extraConfig = ''
    '';
    # Description: Configuration appended to the generated NetworkManager.conf. Refer to https://developer.gnome.org/NetworkManager/stable/NetworkManager.conf.html or NetworkManager.conf 5 for more information.
    # Default value: ""
    # Type: strings concatenated with "\n"
    # Example value: Not given
    # Declared in: nixos/modules/services/networking/networkmanager.nix

    wifi.powersave = true;
    # Description: Whether to enable Wi-Fi power saving. 
    # Default value: Not given
    # Type: null or boolean
    # Example value: Not given
    # Declared in: nixos/modules/services/networking/networkmanager.nix

    packages = with pkgs; [
      networkmanager-openvpn
    ];
  };
}
