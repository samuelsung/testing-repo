{ pkgs, ... }:

{
  # Sound Support
  sound.enable = true; # enable ALSA
  hardware.pulseaudio = {
    enable = true;
    extraModules = [ pkgs.unstable.pulseaudio-modules-bt ];
    package = pkgs.unstable.pulseaudio;
  };
}
