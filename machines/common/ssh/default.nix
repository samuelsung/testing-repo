{ lib, ... }:

let
  inherit (lib) concatStringsSep;
  macs = [
    "umac-128-etm@openssh.com"
    "hmac-sha2-256-etm@openssh.com"
    "hmac-sha2-512-etm@openssh.com"
  ];

  kexAlgorithms = [
    "diffie-hellman-group14-sha256"
    "diffie-hellman-group16-sha512"
    "diffie-hellman-group18-sha512"
    "diffie-hellman-group-exchange-sha256"
    "curve25519-sha256"
    "curve25519-sha256@libssh.org"
  ];

  hostKeyAlgorithms = [
    "rsa-sha2-512"
    "rsa-sha2-256"
    "ssh-ed25519"
  ];
in
{
  # ssh
  # Name          programs.ssh.askPassword
  # Description   Program used by SSH to ask for passwords.
  # Default value /nix/store/znw85gjgp4147m43ra3cx3s45g7dai68-x11-ssh-askpass-1.2.4.1/libexec/x11-ssh-askpass
  # Type          string
  # Example       Not given
  # Declared in   nixos/modules/programs/ssh.nix
  # disable gui ask password, so annoying
  programs.ssh = {
    inherit hostKeyAlgorithms kexAlgorithms macs;
    askPassword = "";

    forwardX11 = false;
  };

  services.openssh = {
    inherit kexAlgorithms macs;

    forwardX11 = false;
    enable = true;
    ports = [ 939 ]; # use port 939 instead of 22
    permitRootLogin = "no";
    passwordAuthentication = false;

    extraConfig = ''
      Protocol 2

      # Auth
      PubkeyAuthentication yes
      ChallengeResponseAuthentication no
      KerberosAuthentication no
      GSSAPIAuthentication no

      ## Default is disable, just to be safe
      IgnoreRhosts yes
      PermitEmptyPasswords no
      MaxAuthTries 3
      MaxSessions 2
      ClientAliveCountMax 2

      # Disable unuse service
      AllowTcpForwarding no
      AllowAgentForwarding no
      PermitTunnel no

      HostKeyAlgorithms ${concatStringsSep "," hostKeyAlgorithms}
    '';
  };
}
