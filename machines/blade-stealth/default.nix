{ ... }:

{
  imports = [
    ./boot.nix
    ./file-system.nix
  ];
}
