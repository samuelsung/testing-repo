{ ... }:

{
  imports = [
    ./boot.nix
    ./file-system.nix
    ./graphics.nix
    ./monitors.nix
    ./sound.nix
  ];
}
