{ ... }:

{
  hardware.pulseaudio.daemon.config = {
    default-sample-channels = 2;
  };
}
