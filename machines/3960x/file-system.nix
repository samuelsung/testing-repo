{ config, ... }:

{
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/608d1f01-173c-41bb-ad52-06d3ec3e9ee3";
    fsType = "ext4";
    options = [ "acl" ];
  };

  fileSystems."/mnt/old" = {
    device = "/dev/disk/by-uuid/fe6479bb-a4bb-4803-99d7-8af5c137c789";
    fsType = "ext4";
    options = [ "acl" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/BEB0-1AD9";
    fsType = "vfat";
  };

  sops.secrets."home_vault_secret" = {
    sopsFile = ../common/secrets/home.home-secret.yaml;
  };

  fileSystems."/mnt/vault" = {
    device = "//10.89.90.20/Main_Set";
    fsType = "cifs";
    options =
      let
        # this line prevents hanging on network split
        # automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
        # automount_opts = "uid=33,gid=33,rw,nounix,iocharset=utf8,file_mode=0777,dir_mode=0777";
        automount_opts = "uid=1000,gid=1000,rw,iocharset=utf8,file_mode=0777,dir_mode=0777";
      in
      [ "${automount_opts},credentials=${config.sops.secrets."home_vault_secret".path}" ]; # Remember to change the secret to 600 and root
  };

  fileSystems."/mnt/mypassport" = {
    device = "/dev/disk/by-uuid/1db24dea-ae15-42f8-b5f7-04484b91d9d9";
    fsType = "ext4";
  };

  swapDevices = [ ];
}
