{ config, ... }:

{
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/d2277dd6-346d-4f8c-9ef3-9146337e83c5";
    fsType = "ext4";
    options = [ "acl" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/3692-089C";
    fsType = "vfat";
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/12cc8b9d-79c2-4853-ad6b-903b19c8f059"; }
  ];

  sops.secrets."vault_secret" = {
    format = "binary";
    mode = "0400";
    sopsFile = ./vault_secret.garemarudo-host-secret;
  };

  fileSystems."/home/samuelsung/vault" = {
    device = "//ifsf04/project";
    fsType = "cifs";
    options = let
      # this line prevents hanging on network split
      # automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
      # automount_opts = "uid=33,gid=33,rw,nounix,iocharset=utf8,file_mode=0777,dir_mode=0777";
      automount_opts = "uid=1000,gid=1000,rw,iocharset=utf8,file_mode=0777,dir_mode=0777";
    in ["${automount_opts},credentials=${config.sops.secrets."vault_secret".path}"]; # Remember to change the secret to 600 and root
  };
}
