{ ... }:

{
  # services.xserver.drivers = [ "modesetting" ];
  # services.xserver.deviceSection = ''
  #   Option "DRI" "2"
  #   Option "TearFree" "true"
  # '';

  services.xserver = {
      videoDrivers = [ "intel" ];
      deviceSection = ''
        # Option "DRI" "2"
        Option "TearFree" "true"
      '';
      # useGlamor = true;

  };
  services.xserver.xrandrHeads = [
    {
      output = "HDMI1";
      primary = true;
      monitorConfig = ''
        Option "PreferredMode" "1920x1080"
        Option "Position" "0 0"
      '';
    }
    {
      output = "DP1";
      primary = false;
      monitorConfig = ''
        Option "PreferredMode" "1920x1080"
        Option "Position" "1920 0"
      '';
    }
  ];
}
