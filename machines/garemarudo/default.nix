{ ... }:

{
  imports = [
    ./boot.nix
    ./file-system.nix
    ./monitors.nix
  ];
}
