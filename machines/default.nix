{ nixos-hardware
, nixosSystem
, nixpkgs
, nixpkgs-master
, nixpkgs-unstable
, nixpkgs-alt-overlay
, nur
, sops-nix
, custom-package-overlay
}:

let
  common = import ./common;
  modules = import ../modules;

  common-modules = [
    ({ ... }: {
      nixpkgs.overlays = [
        nixpkgs-alt-overlay
        custom-package-overlay
        nur.overlay
      ];
    })
    modules.nix-unfree
    sops-nix.nixosModules.sops
    (modules.sops-nix { })
  ];

  cli-common-modules = with modules; [
    console
    nix-flake
    nix-garbage-collection
    # nix-unstable
    utils-cli
    zsh

    (secret-scripts { username = "samuelsung"; })
  ];

  cli-desktop-modules = with modules; [
    utils-cli-desktop
    modules.privoxy
    (lab { username = "samuelsung"; })
    (lab { username = "samuelsung_work"; })
  ];

  cli-dev-modules = with modules; [
    utils-cli-development
  ];

  gui-common-modules = with modules; [
    dconf
    fcitx5
    fonts
    gnome-keyring
    # ibus
    utils-gui
    xmonad
  ];

  gui-dev-modules = with modules; [
    utils-gui-development
  ];
in
{
  tr3960x = nixosSystem {
    system = "x86_64-linux";
    modules = common-modules
      ++ cli-common-modules
      ++ cli-desktop-modules
      ++ cli-dev-modules
      ++ gui-common-modules
      ++ [
      (modules.system-info {
        hostName = "tr3960x";
        stateVersion = "20.09";
      })

      (modules.users {
        with-libvirtd-group = true;
        with-docker-group = true;
      })

      (modules.secret-sshkeys {
        sopsFile = ../home/samuelsung/ssh/keys/keys.tr3960x-host-secret.json;
        publicKeyFiles = [
          ../home/samuelsung/ssh/keys/public-keys/tr3960x
          ../home/samuelsung/ssh/keys/public-keys.old/tr3960x
        ];
        key = "ssh";
        users = ["samuelsung" "samuelsung_work"];
      })

      (modules.secret-sshkeys {
        sopsFile = ../home/samuelsung/ssh/keys/keys.home-host-secret.json;
        key = "ssh";
        users = ["samuelsung" "samuelsung_work"];
      })

      (modules.secret-agekeys {
        sopsFile = ../home/samuelsung/age/keys/keys.tr3960x-host-secret.json;
        key = "age";
        users = ["samuelsung" "samuelsung_work"];
      })

      (modules.secret-firefox-bookmark { username = "samuelsung"; })

      (import ./3960x)
    ] ++ (with common; [
      bluetooth
      (firewall {
        allowedTCPPorts = [ 3000 ];
      })
      networkmanager
      ntp
      samba
      sound
      ssh
      touchpad
      wireguard
    ]);
  };

  blade-stealth = nixosSystem {
    system = "x86_64-linux";
    modules = common-modules
      ++ cli-common-modules
      ++ cli-desktop-modules
      ++ cli-dev-modules
      ++ gui-common-modules
      ++ gui-dev-modules
      ++ [
      (modules.system-info {
        hostName = "blade-stealth";
        stateVersion = "21.05";
      })

      (modules.users {
        with-libvirtd-group = true;
        with-docker-group = true;
        with-video-group = true;
      })

      (modules.secret-sshkeys {
        sopsFile = ../home/samuelsung/ssh/keys/keys.blade-stealth-host-secret.json;
        publicKeyFiles = [
          ../home/samuelsung/ssh/keys/public-keys/blade-stealth
          ../home/samuelsung/ssh/keys/public-keys.old/blade-stealth
        ];
        key = "ssh";
        users = ["samuelsung" "samuelsung_work"];
      })

      (modules.secret-sshkeys {
        sopsFile = ../home/samuelsung/ssh/keys/keys.home-host-secret.json;
        key = "ssh";
        users = ["samuelsung" "samuelsung_work"];
      })

      modules.light

      (modules.secret-firefox-bookmark { username = "samuelsung"; })

      (import ./blade-stealth)
    ] ++ (with common; [
      bluetooth
      brightness-control
      (firewall {
        allowedTCPPorts = [ 3000 ];
      })
      networkmanager
      ntp
      samba
      sound
      ssh
      touchpad
      wireguard
    ]);
  };

  xps-15-9500 = nixosSystem {
    system = "x86_64-linux";
    modules = common-modules
      ++ cli-common-modules
      ++ cli-desktop-modules
      ++ cli-dev-modules
      ++ gui-common-modules
      ++ gui-dev-modules
      ++ [
      (modules.system-info {
        hostName = "xps-15-9500";
        stateVersion = "21.05";
      })

      (modules.users {
        with-libvirtd-group = true;
        with-docker-group = true;
        with-video-group = true;
      })

      (modules.secret-sshkeys {
        sopsFile = ../home/samuelsung/ssh/keys/keys.xps-15-9500-host-secret.json;
        publicKeyFiles = [
          ../home/samuelsung/ssh/keys/public-keys/xps-15-9500
          ../home/samuelsung/ssh/keys/public-keys.old/xps-15-9500
        ];
        key = "ssh";
        users = ["samuelsung" "samuelsung_work"];
      })

      (modules.secret-sshkeys {
        sopsFile = ../home/samuelsung/ssh/keys/keys.home-host-secret.json;
        key = "ssh";
        users = ["samuelsung" "samuelsung_work"];
      })

      modules.light

      (modules.secret-firefox-bookmark { username = "samuelsung"; })

      (import ./xps-15-9500)
      nixos-hardware.nixosModules.dell-xps-15-9500-nvidia
    ] ++ (with common; [
      bluetooth
      brightness-control
      (firewall {
        allowedTCPPorts = [ 3000 ];
      })
      networkmanager
      ntp
      samba
      sound
      ssh
      touchpad
      wireguard
    ]);
  };

  garemarudo = nixosSystem {
    system = "x86_64-linux";
    modules = common-modules
      ++ cli-common-modules
      ++ cli-desktop-modules
      ++ cli-dev-modules
      ++ gui-common-modules
      ++ gui-dev-modules
      ++ [
      (modules.system-info {
        hostName = "garemarudo";
        stateVersion = "20.09";
      })

      (modules.users {
        with-libvirtd-group = true;
        with-docker-group = true;
      })

      (modules.secret-sshkeys {
        sopsFile = ../home/samuelsung/ssh/keys/keys.garemarudo-host-secret.json;
        publicKeyFiles = [
          ../home/samuelsung/ssh/keys/public-keys/garemarudo
          ../home/samuelsung/ssh/keys/public-keys.old/garemarudo
        ];
        key = "ssh";
        users = ["samuelsung" "samuelsung_work"];
      })

      (modules.secret-agekeys {
        sopsFile = ../home/samuelsung/age/keys/keys.garemarudo-host-secret.json;
        key = "age";
        users = ["samuelsung" "samuelsung_work"];
      })

      (import ./garemarudo)
    ] ++ (with common; [
      bluetooth
      brightness-control
      (firewall {
        allowedTCPPorts = [ 8080 9000 ];
      })
      networkmanager
      ntp
      opengl-intel
      samba
      sound
      ssh
      touchpad
      wireguard
    ]);
  };

  reverseproxy = nixosSystem {
    system = "x86_64-linux";
    modules = common-modules
      ++ cli-common-modules
      ++ [
      (modules.system-info {
        hostName = "reverseproxy";
        stateVersion = "20.09";
      })

      (modules.users { })

      (modules.server-reverseproxy {
        targets = [
          {
            address = "bitwarden.samuelsung1998.net";
            target = "http://10.89.90.26:8000";
          }
          {
            address = "jellyfin.samuelsung1998.net";
            target = "http://10.89.90.28:8096";
          }
          {
            address = "nextcloud.samuelsung1998.net";
            target = "http://10.89.90.21:80";
          }
        ];
      })

      (import ./blade-stealth) # TODO: add reverseproxy
    ] ++ (with common; [
      (firewall { })
      networkmanager
      ntp
      ssh
    ]);
  };
}
