{
  description = "personal NixOS configurations with flakes";

  inputs = {
    dmenu.url = "gitlab:samuelsung/dmenu";

    dwm.url = "gitlab:samuelsung/dwm";

    final-fantasy-xiv-sync.url = "gitlab:samuelsung/final-fantasy-xiv-sync";

    flake-utils.url = "github:numtide/flake-utils";

    home-manager.url = "github:nix-community/home-manager/release-21.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    nixpkgs.url = "nixpkgs/nixos-21.05";
    nixpkgs-master.url = "nixpkgs/master";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";

    nur.url = "github:nix-community/NUR";

    slock.url = "gitlab:samuelsung/slock";

    sops-nix.url = "github:Mic92/sops-nix";

    st.url = "gitlab:samuelsung/st";
  };

  outputs = args: import ./outputs.nix args;
}
