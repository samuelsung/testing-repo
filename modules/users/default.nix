{ with-libvirtd-group ? false
, with-docker-group ? false
, with-video-group ? false
}:
{ pkgs, ... }:

let
  samuelsung_common = {
    isNormalUser = true;
    initialPassword = "p@ssw0rd";
    extraGroups = [
      "wheel" # Enable ‘sudo’ for the user.
      "networkmanager" # Enable ‘networkmanager’ for the user.
      "samuelsung_common"
    ] ++
    # Enable kvm control
    (if with-libvirtd-group then [ "libvirtd" ] else [ ])
    ++
    # Enable docker control
    (if with-docker-group then [ "docker" ] else [ ])
    ++
    # Enable docker control
    (if with-video-group then [ "video" ] else [ ]);

    # TODO(samuelsung): Remove this after finish all migration
    openssh.authorizedKeys.keys = import ../../home/samuelsung/ssh/authorizedKeys.nix;
  };
in
{
  users = {
    users.samuelsung = samuelsung_common;

    users.samuelsung_work = samuelsung_common;

    users.root = {
      initialPassword = "p@ssw0rd";
    };

    mutableUsers = true;
    defaultUserShell = pkgs.zsh;

    groups = {
      samuelsung_common = { };
    };
  };

  # Make a shared foler 'repos' for storing repositories
  system.activationScripts.mkRepos = ''
    mk_share_folder() {
      folder_path="$1";
      folder_base="$(basename $folder_path)"

      [ -d "$folder_path" ] || mkdir "$folder_path";
      chmod 2770 "$folder_path";
      chgrp -R samuelsung_common "$folder_path";

      ${pkgs.acl}/bin/setfacl -R -d -m u::rwx,g::rwx,o::r-x "$folder_path" || echo "skip setting $folder_path acl";

      ln_as_user() {
        user="$1";

        [ -e "/home/$user/$folder_base" ] || ${pkgs.sudo}/bin/sudo -u "$user" ln -s "$folder_path" "/home/$user";
      }

      ln_as_user samuelsung;
      ln_as_user samuelsung_work;
    }

    mk_share_folder "/srv/music";
    mk_share_folder "/srv/repos";
  '';
}
