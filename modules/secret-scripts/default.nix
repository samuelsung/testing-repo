{ username }:

{ config, ... }:

{
  sops.secrets."helper" = {
    format = "binary";
    sopsFile = ./helper.secret.sh;
    mode = "0550";
    owner = username;
    path = "/home/${username}/.local/bin/helper";
  };
}
