{ pkgs, ... }:

{
  services.gitlab = {
    enable = true;
    https = true;
    port = 443;
    host = "gitlab.samuelsung1998.net";

    user = "git";
    databaseUsername = "git";

    initialRootPasswordFile = toString ../secrets/gitlab-initial-root-password;

    # can be generated by `tr -dc 'A-Za-z0-9' </dev/urandom | head -c 128`
    secrets.otpFile = toString ../secrets/gitlab-otp-key;
    secrets.dbFile = toString ../secrets/gitlab-db-key;
    secrets.secretFile = toString ../secrets/gitlab-secret;

    # can be generated by `openssl genrsa 4096`
    secrets.jwsFile = toString ../secrets/gitlab-jws-key;
  };

  networking.firewall = {
    allowedTCPPorts = [ 80 443 ];
  };

  security.acme.email = "sunghoyin@samuelsung1998.net";

  security.acme.acceptTerms = true;

  services.nginx = {
    enable = true;

    upstreams = {
      "gitlab-workhorse" = {
        servers = {
          "unix:/run/gitlab/gitlab-workhorse.socket fail_timeout=0" = {
          };
        };
      };
    };

    virtualHosts = let
      SSL = {
        enableACME = true;
        forceSSL = true;
      }; in {
        "gitlab.samuelsung1998.net" = (SSL // {
          locations."~ ^/(assets)/" = {
            root = ''${pkgs.gitlab}/share/gitlab/public'';
            extraConfig = ''
              gzip_static on; # to serve pre-gzipped version
              expires max;
              add_header Cache-Control public;
            '';
          };

          locations."/" = {
            proxyPass = "http://gitlab-workhorse";
            extraConfig = ''
              # unlimited upload size in nginx (so the setting in GitLab applies)
              client_max_body_size 0;

              # proxy timeout should match the timeout value set in /etc/webapps/gitlab/puma.rb
              proxy_read_timeout 60;
              proxy_connect_timeout 60;
              proxy_redirect off;

              proxy_set_header Host $host;
              proxy_set_header X-Real-IP $remote_addr;
              proxy_set_header X-Forwarded-Ssl on;
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header X-Forwarded-Proto $scheme;
            '';
          };

          extraConfig = '' 
            error_page 404 /404.html;
            error_page 422 /422.html;
            error_page 500 /500.html;
            error_page 502 /502.html;
            error_page 503 /503.html;
          '';

          locations."~ ^/(404|422|500|502|503)\.html$" = {
            root = ''${pkgs.gitlab}/share/gitlab/public'';
            extraConfig = "internal;";
          };
        });

        # "sub.domain.tld" = (SSL // {
        #   locations."/".proxyPass = "http://127.0.0.1:8081/";
        # });
      };
  };
}

