{ sopsFile, users, key }:

{ lib, ... }:

let
  inherit (lib) mapAttrs' nameValuePair foldr;
  # title = toString sopsFile;
  encryptedJson = (builtins.fromJSON (
    builtins.readFile sopsFile
  ))."${key}";
in
{
  sops.secrets =
    foldr
      (cur: acc: acc // {
        "sops-age-${cur}" = {
          inherit sopsFile key;
          owner = cur;
          path = "/home/${cur}/.config/sops/age/keys.txt";

          mode = "0400";
        };
      })
      { }
      users;
}
