{ port ? 8000 }:
{ pkgs, config, lib, ... }:

{
  networking.firewall.allowedTCPPorts = [ port ];

  services.bitwarden_rs = {
    enable = true;
    config = {
      rocketPort = port;
      signupsAllowed = false;
    };
  };
}
