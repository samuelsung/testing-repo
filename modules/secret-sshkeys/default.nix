{ sopsFile
, users
, key
, publicKeyFiles ? [ ]
}:

{ lib, ... }:

let
  inherit (lib) mapAttrs' nameValuePair foldr;
  # title = toString sopsFile;
  encryptedJson = (builtins.fromJSON (
    builtins.readFile sopsFile
  ))."${key}";
in
{
  sops.secrets =
    foldr
      (cur: acc: acc // (mapAttrs'
        (k: _: nameValuePair "ssh-${cur}-${k}" {
          inherit sopsFile;
          owner = cur;
          key = "${key}/${k}/private";
          path = "/home/${cur}/.ssh/${k}";

          mode = "0400";
        })
        encryptedJson
      ) // (mapAttrs'
        (k: _: nameValuePair "ssh-${cur}-${k}-pub" {
          inherit sopsFile;
          owner = cur;
          key = "${key}/${k}/public";
          path = "/home/${cur}/.ssh/${k}.pub";

          mode = "0444";
        })
        encryptedJson
      ))
      { }
      users;

  users.users =
    foldr
      (cur: acc: acc // {
        "${cur}" = {
          openssh.authorizedKeys.keyFiles = publicKeyFiles;
        };
      })
      { }
      users;
}
