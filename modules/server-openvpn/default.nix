{ ext-dev
, vpn-dev ? "tun0"
# generate via openvpn --genkey --secret openvpn-laptop.key
, client-key ? "/root/openvpn.key"
, port ? 1194
, server-key-name ? "server"
, client-key-name ? "client"
}:

{ pkgs, config, lib, ... }:

let
  genOvpn = pkgs.writeShellScriptBin "genOvpn" ''
    currentIp=$(ip address show "${interface}" | grep "inet" | head --lines 1 | awk '{print $2}' | cut -f1 -d'/');

    echo "dev tun"
    echo "proto udp"

    echo "remote $currentIp ${toString port}"

    echo "ca ca.crt"
    echo "cert ${client-key-name}.crt"
    echo "key ${client-key-name}.key"

    echo "cipher AES-256-CBC"
    echo "auth SHA512"
    echo "auth-nocache"
    echo "tls-version-min 1.2"
    echo "tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256:TLS-DHE-RSA-WITH-AES-128-GCM-SHA256:TLS-DHE-RSA-WITH-AES-128-CBC-SHA256"

    echo "resolv-retry infinite"
    echo "compress lz4"
    echo "nobind"
    echo "persist-key"
    echo "persist-tun"
    echo "mute-replay-warnings"
    echo "verb 3"
  '';

  genCrt = pkgs.writeShellScriptBin "getCrt" ''
    PATH=$PATH:${lib.makeBinPath [ pkgs.gawk pkgs.which pkgs.openssl pkgs.easyrsa pkgs.openvpn ]}

    [ -f "${client-key}" ] || {
      echo "create secret key"
      openvpn --genkey --secret ${client-key}
    }

    EASYRSA_VARS_FILE="/etc/easyrsa/vars"
    [ -d "/etc/easyrsa" ] || mkdir -p /etc/easyrsa
    [ -d "/etc/openvpn/server" ] || mkdir -p /etc/openvpn/server
    [ -d "/etc/openvpn/client" ] || mkdir -p /etc/openvpn/client

    cd /etc/easyrsa

    [ -d "/etc/easyrsa/pki" ] || easyrsa init-pki
    [ -f "/etc/easyrsa/pki/ca.crt" ] || {
      easyrsa build-ca

      [ -f "/etc/easyrsa/pki/ca.crt" ] && {
        rm -rf /etc/openvpn/server/ca.crt
        cp /etc/easyrsa/pki/ca.crt /etc/openvpn/server

        rm -rf /etc/openvpn/client/ca.crt
        cp /etc/easyrsa/pki/ca.crt /etc/openvpn/client
      }
    }

    [ -f "/etc/easyrsa/pki/issued/${server-key-name}.crt" ] || {
      ([ -f "/etc/easyrsa/pki/private/${server-key-name}.key" ] && [ -f "/etc/easyrsa/pki/reqs/${server-key-name}.req" ]) || easyrsa gen-req ${server-key-name} nopass
      [ -f "/etc/easyrsa/pki/reqs/${server-key-name}.req" ] && easyrsa sign-req server ${server-key-name}

      [ -f "/etc/easyrsa/pki/issued/${server-key-name}.crt" ] && {
        rm -rf /etc/openvpn/server/${server-key-name}.crt
        cp /etc/easyrsa/pki/issued/${server-key-name}.crt /etc/openvpn/server

        rm -rf /etc/openvpn/server/${server-key-name}.key
        cp /etc/easyrsa/pki/private/${server-key-name}.key /etc/openvpn/server
      }
    }

    openssl verify -CAfile pki/ca.crt pki/issued/${server-key-name}.crt

    [ -f "/etc/easyrsa/pki/issued/${client-key-name}.crt" ] || {
      ([ -f "/etc/easyrsa/pki/private/${client-key-name}.key" ] && [ -f "/etc/easyrsa/pki/reqs/${client-key-name}.req" ]) || easyrsa gen-req ${client-key-name} nopass
      [ -f "/etc/easyrsa/pki/reqs/${client-key-name}.req" ] && easyrsa sign-req client ${client-key-name}

      [ -f "/etc/easyrsa/pki/issued/${client-key-name}.crt" ] && {
        rm -rf /etc/openvpn/client/${client-key-name}.crt
        cp /etc/easyrsa/pki/issued/${client-key-name}.crt /etc/openvpn/client

        rm -rf /etc/openvpn/client/${client-key-name}.key
        cp /etc/easyrsa/pki/private/${client-key-name}.key /etc/openvpn/client
      }
    }

    openssl verify -CAfile pki/ca.crt pki/issued/${client-key-name}.crt

    [ -f "/etc/easyrsa/pki/dh.pem" ] || {
      easyrsa gen-dh

      rm -rf /etc/openvpn/server/dh.pem
      cp /etc/easyrsa/pki/dh.pem /etc/openvpn/server
    }
  '';
in {
  # sudo systemctl start nat
  networking.nat = {
    enable = true;
    externalInterface = ext-dev;
    internalInterfaces  = [ vpn-dev ];
  };
  networking.firewall.trustedInterfaces = [ vpn-dev ];
  networking.firewall.allowedUDPPorts = [ port ];

  environment.systemPackages = [
    pkgs.openvpn
    pkgs.easyrsa
    pkgs.openssl
  ]; # for key generation

  services.openvpn.servers.openvpn-server.config = ''
    # OpenVPN Port, Protocol, and the Tun
    port ${toString port}
    proto udp
    dev ${vpn-dev}

    # OpenVPN Server Certificate - CA, server key and certificate
    ca /etc/openvpn/server/ca.crt
    cert /etc/openvpn/server/${server-key-name}.crt
    key /etc/openvpn/server/${server-key-name}.key

    #DH
    dh /etc/openvpn/server/dh.pem

    # Network Configuration - Internal network
    # Redirect all Connection through OpenVPN Server
    server 10.5.0.0 255.255.255.0
    push "redirect-gateway def1"

    # DNS
    push "dhcp-option DNS 1.1.1.1"
    push "dhcp-option DNS 1.0.0.1"

    # Enable multiple clients to connect with the same certificate key
    duplicate-cn

    # TLS Security
    cipher AES-256-CBC
    tls-version-min 1.2
    tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256:TLS-DHE-RSA-WITH-AES-128-GCM-SHA256:TLS-DHE-RSA-WITH-AES-128-CBC-SHA256
    auth SHA512
    auth-nocache

    # Other Configuration
    keepalive 20 60
    persist-key
    persist-tun
    compress lz4
    daemon
    user nobody
    group nobody

    # OpenVPN Log
    log-append /var/log/openvpn.log
    verb 3
  '';

  environment.etc."easyrsa/vars" = {
    text = ''
      set_var EASYRSA_DN              "cn_only"
      set_var EASYRSA_REQ_COUNTRY     "HK"
      set_var EASYRSA_REQ_PROVINCE    "Hong_Kong"
      set_var EASYRSA_REQ_CITY        "Hong_Kong"
      set_var EASYRSA_REQ_ORG         "samuelsung vultr openvpn CERTIFICATE AUTHORITY"
      set_var EASYRSA_REQ_EMAIL       "sunghoyin@samuelsung1998.net"
      set_var EASYRSA_REQ_OU          "samuelsung vultr openvpn CA"
      set_var EASYRSA_KEY_SIZE        4096
      set_var EASYRSA_ALGO            rsa
      set_var EASYRSA_CA_EXPIRE       7500
      set_var EASYRSA_CERT_EXPIRE     365
      set_var EASYRSA_NS_SUPPORT      "no"
      set_var EASYRSA_NS_COMMENT      "samuelsung vultr openvpn CERTIFICATE AUTHORITY"
      set_var EASYRSA_DIGEST          "sha512"
    '';
    mode = "555";
  };
}

