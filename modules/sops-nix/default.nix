{}:
{ ... }:

{
  sops.defaultSopsFile = ../../common.secret.yaml;
  sops.age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
}
