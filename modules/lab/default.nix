{ username }:

{ pkgs, config, ... }:

{
  environment.systemPackages = with pkgs; [
    unstable.lab
  ];

  # Expiry: Dec 31, 2022 8:00am GMT+0800
  sops.secrets."lab-${username}" = {
    path = "/home/${username}/.config/lab/lab.toml";
    format = "binary";
    sopsFile = ./lab.desktop-secret;
    mode = "0400";
    owner = username;
  };
}
