{ username }:

{ config, ... }:

{
  sops.secrets."bookmark.html" = {
    format = "binary";
    sopsFile = ./bookmarks.secret;
    mode = "0700";
    owner = username;
  };
}
