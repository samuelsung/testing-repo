{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # webcam
    v4l-utils # control webcam params

    # multimedia
    pulsemixer
    (ffmpeg-full.override { # ffmpeg with libbluray enabled
      libbluray = pkgs.libbluray.override {
        withAACS = true;
        withBDplus = true;
      };
    })

    # mount
    ntfs3g # NTFS
    fuse # NTFS
    udisks # mount usb
  ];
}
