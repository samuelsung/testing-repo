{ targets }:

{ ... }:

let
  inherit (builtins) listToAttrs map;
in
{
  networking.firewall = {
    allowedTCPPorts = [ 80 443 ];
  };

  security.acme.email = "sunghoyin@samuelsung1998.net";

  security.acme.acceptTerms = true;

  services.nginx = {
    enable = true;

    virtualHosts =
      let
        common = {
          locations."/" = {
            extraConfig = ''
              # unlimited upload size in nginx (so the setting in application applies)
              client_max_body_size 0;

              # proxy timeout should match the timeout value set in /etc/webapps/gitlab/puma.rb
              proxy_read_timeout 60;
              proxy_connect_timeout 60;
              proxy_redirect off;

              proxy_set_header Host $host;
              proxy_set_header X-Real-IP $remote_addr;
              proxy_set_header X-Forwarded-Ssl on;
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header X-Forwarded-Proto $scheme;
            '';
          };

        };
        SSL = {
          enableACME = true;
          forceSSL = true;
        };
      in
      (listToAttrs (
        map
          ({ address, target }: {
            name = address;
            value = (SSL // common // {
              locations."/".proxyPass = target;
            });
          })
          targets
      ));
  };
}
