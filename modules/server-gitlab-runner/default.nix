{ pkgs, ... }:

{
  virtualisation.docker.enable = true;

  services.gitlab-runner = {
    enable = true;
    concurrent = 10;

    services = {
      "gitlab.samuelsung1998.net" = {
        executor = "docker";
        maximumTimeout = 3600;
        environmentVariables = {
          RUNNER = "local-gitlab-runner";
        };
        runUntagged = true;
        registrationConfigFile = "/etc/nixos/secrets/gitlab.samuelsung1998.net.registration-config";
        dockerImage = "nixos/nix";
      };
    };
  };
}
