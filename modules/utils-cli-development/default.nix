{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # utils
    unstable.glab # gitlab cli tool (only available on unstable as of 2021 Mar 10)

    # management
    linode-cli

    # LaTeX
    texlive.combined.scheme-full

    # C
    gcc
    gnumake
    gnupatch
    libtool
    autoconf
    automake
    pkgconf
    texinfo
    binutils

    # Javascript
    nodejs
    yarn
    nodePackages.expo-cli

    # Python
    python3Full # Note that there is no need to ust pip in Nixos https://nixos.wiki/wiki/Python

    # Rust
    cargo
    rustc
    rustup

    # Go
    go

    # Java
    # jdtls

    # sql
    sqlite

    # docker
    docker-compose

    # git wiki
    gollum

    # android
    android-studio

    # Elm
    elmPackages.elm

    # Haskell
    haskellPackages.haskell-language-server
    haskellPackages.cabal-install
    ghc
  ];

  # docker
  virtualisation.docker.enable = true;

  # adb
  programs.adb.enable = true;

  # Java
  programs.java = {
    enable = true;
    package = pkgs.openjdk11;
  };
}
