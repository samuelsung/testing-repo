{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # Sound
    pulsemixer
    playerctl
  ];
}
