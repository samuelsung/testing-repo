{ with-looking-glass ? false
, with-iscsi ? false
}:

{ pkgs, config, lib, ... }:

(
  if with-iscsi then {
    nixpkgs.config.packageOverrides = pkgs: {
      libvirt = pkgs.libvirt.override { enableIscsi = true; };
    };

    environment.systemPackages = with pkgs; [
      openiscsi
    ];

    systemd.services.iscsid = {
      description = "iSCSI daemon";
      path = [ pkgs.openiscsi ];
      script = "iscsid -f";
      wantedBy = [ "multi-user.target" ];
    };

  } else { }
) // (
  if with-looking-glass then {
    environment.systemPackages = with pkgs; [
      (scream.override {
        pulseSupport = true;
      })

      unstable.looking-glass-client
    ];
    networking.firewall.allowedUDPPorts = [
      4010 ## for scream audio
    ];
  } else { }
) // {
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;
}
