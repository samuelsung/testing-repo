{ pkgs, ... }:

{
  # Currently flake only work on unstable nix
  # and need to be enabled explicitly
  imports = [ ../nix-unstable ];
  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
}
