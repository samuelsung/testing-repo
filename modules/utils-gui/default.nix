{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # makemkv

    # X related utils
    xsel
    xorg.xwininfo
    xorg.xdpyinfo
    xorg.xbacklight
    xclip
    xdotool
    unclutter-xfixes # hide cursor if inactive
    slock
    xss-lock

    # Remote
    remmina
    libvncserver
    freerdp

    # Commutation
    teams
    master.discord
    zoom-us
    signal-desktop

    # Mail
    protonmail-bridge
    thunderbird

    # Configurations
    lxappearance
    lxrandr
    xlayoutdisplay

    # Browser
    firefox
    ungoogled-chromium

    # Terminal
    xterm
    alacritty

    # File Manager
    gnome3.nautilus
    calibre
    custom.vifmimg

    # Image Viewer
    sxiv

    # Documents
    zathura
    libreoffice
    (unstable.joplin-desktop.override {
      appimageTools = pkgs.appimageTools.override {
        buildFHSUserEnv = pkgs.buildFHSUserEnv;
      };
    })

    # Multimedia
    picard
    deadbeef-with-plugins
    mpv
    mkvtoolnix
    (vlc.override {
      libbluray = pkgs.libbluray.override {
        withAACS = true;
        withBDplus = true;
      };
    })
    handbrake

    # Image
    imagemagick
    ffmpegthumbnailer
    poppler_utils
    fontpreview

    # Design
    drawio

    # Admin
    gnome3.seahorse
  ];

  security.wrappers.slock.source = "${pkgs.slock.out}/bin/slock"; # TODO: move to home-manager
}
