{ lib, config, pkgs, ... }:

let
  inherit (builtins) attrNames;
  inherit (lib) concatStringsSep filterAttrs;
  normalUsers = attrNames (filterAttrs (_: v: v.isNormalUser) config.users.users);
in
{
  programs.light.enable = true;

  system.activationScripts.setLightMinimum = ''
    for u in ${concatStringsSep " " normalUsers}; do {
      ${pkgs.sudo}/bin/sudo -u "$u" ${pkgs.light}/bin/light -N 1 # set minimum brightness
    }; done;
  '';
}
