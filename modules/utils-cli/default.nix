{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # compression
    gzip
    zip
    unzip
    unrar
    dpkg
    cpio

    # utils
    git
    wget
    curl
    jq
    coreutils-full
    dbus # read dbus singal
    pciutils # lspci...
    psmisc # killall, pstree...
    fzf # search
    openssl
    htop
    acl # for file permission
    file # get file type
    p7zip # 7z

    # File Manager
    ranger
    vifm
    (moreutils.overrideAttrs(oldAttrs: rec {
      preBuild = oldAttrs.preBuild + ''
        # add space padding
        substituteInPlace vidir --replace "print OUT \"\$c\t\$_\\n\";" "\$d=sprintf(\"%5d\", \$c); print OUT \"\$d \$_\\n\"";
        substituteInPlace vidir --replace "if (/^(\\d+)\\t{0,1}(.*)/) {" "if(/^\\s*(\\d+) {0,1}(.*)/) {";
      '';
    }))

    # miscellaneous
    termdown # tui timer

    # nixos
    patchelf
    appimage-run
    steam-run
    home-manager
    vgo2nix
    nodePackages.node2nix

    # secrets
    sops
    unstable.ssh-to-age
    age
  ];
}
