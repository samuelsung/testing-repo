{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # Remote
    custom.moonlight-qt
    unstable.looking-glass-client
  ];
}
