{ pkgs, ... }:

{
  nix = {
    package = pkgs.nixUnstable;
  };
}
