{ ... }:

{
  # TODO: make it filter all traffic by befault
  services.privoxy.enable = true;
  # services.privoxy.settings = {
  #   accept-intercepted-requests = 1;
  # };
  services.privoxy.userActions = ''
    {+block{Block youtube}}
    .youtube.

    {+block{Block games}}
    .finalfantasyxiv.
    .game8.

    {+block{Block social media}}
    .twitter.
    .5ch.
  '';
}
