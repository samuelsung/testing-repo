{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    ## Design
    custom.figma

    ## Database
    dbeaver

    ## IDE
    vscodium
  ];
}
