{ ... }:

{
  home.file.".local/share/fcitx5/rime/default.custom.yaml" = {
    force = true;
    source = ./default.custom.yaml;
  };
  home.file.".local/share/fcitx5/themes/nord" = {
    force = true;
    source = ./nord;
  };
  home.file.".config/fcitx5/profile" = {
    force = true;
    source = ./profile;
  };
  home.file.".config/fcitx5/config" = {
    force = true;
    source = ./config;
  };
  home.file.".config/fcitx5/conf/classicui.conf" = {
    force = true;
    source = ./classicui.conf;
  };
}
