{ wallpaper ? ./wallpapers/final-fantasy-xiv-endwalker.png
}: { pkgs, ... }:

{
  home.packages = with pkgs; [
    feh
    networkmanagerapplet
  ];

  home.file.".Xresources".source = ./.Xresources;
  home.file.".xinitrc".source = ./.xinitrc;

  home.file.".config/wallpaper".source = wallpaper;
}
