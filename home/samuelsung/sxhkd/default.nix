{ services, ... }:

{
  services.sxhkd.enable = true;
    # Whether to enable simple X hotkey daemon.
    # Type: boolean
    # Default: false
    # Example: true
    # Declared by:
    # <home-manager/modules/services/sxhkd.nix> 

  services.sxhkd.extraConfig = ''${builtins.readFile ./sxhkdrc}'';
    # Additional configuration to add.
    # Type: strings concatenated with "\n"
    # Default: ""
    # Example:
    # super + {_,shift +} {1-9,0}
    #   i3-msg {workspace,move container to workspace} {1-10}
    # Declared by:
    # <home-manager/modules/services/sxhkd.nix> 

  # services.sxhkd.extraPath
    # Additional PATH entries to search for commands.
    # Type: strings concatenated with ":"
    # Default: ""
    # Example: "/home/some-user/bin:/extra/path/bin"
    # Declared by:
    # <home-manager/modules/services/sxhkd.nix> 

  # services.sxhkd.keybindings
    # An attribute set that assigns hotkeys to commands.
    # Type: attribute set of null or strings
    # Default: { }
    # Example:
    # {
    #   "super + shift + {r,c}" = "i3-msg {restart,reload}";
    #   "super + {s,w}"         = "i3-msg {stacking,tabbed}";
    # }
    # Declared by:
    # <home-manager/modules/services/sxhkd.nix> 
}
