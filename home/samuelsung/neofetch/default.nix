{ pkgs, ... }:

{
  home.packages = with pkgs; [
    pfetch
    neofetch
  ];

  home.file.".config/neofetch/config.conf".source = ./config.conf;
}
