{ config, pkgs, ... }:

{
  programs.tmux.enable = true;
    # Whether to enable tmux.
    # Type: boolean
    # Default: false
    # Example: true
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.package = pkgs.tmux;
    # The tmux package to install
    # Type: package
    # Default: pkgs.tmux
    # Example:
    # pkgs.tmux
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.aggressiveResize = false;
    # Resize the window to the size of the smallest session for which it is the current window.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.baseIndex = 1;
    # Base index for windows and panes.
    # Type: unsigned integer, meaning >=0
    # Default: 0
    # Example: 1
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.clock24 = false;
    # Use 24 hour clock.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.customPaneNavigationAndResize = false;
    # Override the hjkl and HJKL bindings for pane navigation and resizing in VI mode.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.disableConfirmationPrompt = false;
    # Disable confirmation prompt before killing a pane or window
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.escapeTime = 0;
    # Time in milliseconds for which tmux waits after an escape is input.
    # Type: unsigned integer, meaning >=0
    # Default: 500
    # Example: 0
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.extraConfig =
    # Additional configuration to add to tmux.conf.
    # Type: strings concatenated with "\n"
    # Default: ""
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>
  ''
    # C-b is not acceptable -- Vim uses it
    set-option -g prefix C-a
    bind-key C-a last-window

    bind h select-pane -L
    bind n select-pane -D
    bind e select-pane -U
    bind i select-pane -R

    bind C-i next-window
    bind C-h previous-window

    set-option -sa terminal-overrides ',st-256color:RGB'

    set-window-option -g mode-keys vi
    if-shell "test '\( #{$TMUX_VERSION_MAJOR} -eq 2 -a #{$TMUX_VERSION_MINOR} -ge 4 \)'" 'bind-key -Tcopy-mode-vi v send -X begin-selection; bind-key -Tcopy-mode-vi y send -X copy-selection-and-cancel'
    if-shell '\( #{$TMUX_VERSION_MAJOR} -eq 2 -a #{$TMUX_VERSION_MINOR} -lt 4\) -o #{$TMUX_VERSION_MAJOR} -le 1' 'bind-key -t vi-copy v begin-selection; bind-key -t vi-copy y copy-selection'

    # rm mouse mode fail
    if-shell '\( #{$TMUX_VERSION_MAJOR} -eq 2 -a #{$TMUX_VERSION_MINOR} -ge 1\)' 'set -g mouse off'
    if-shell '\( #{$TMUX_VERSION_MAJOR} -eq 2 -a #{$TMUX_VERSION_MINOR} -lt 1\) -o #{$TMUX_VERSION_MAJOR} -le 1' 'set -g mode-mouse off'

    # fix pane_current_path on new window and splits
    unbind \"
    unbind %
    if-shell "test '#{$TMUX_VERSION_MAJOR} -gt 1 -o \( #{$TMUX_VERSION_MAJOR} -eq 1 -a #{$TMUX_VERSION_MINOR} -ge 8 \)'" 'unbind c; bind c new-window -c "#{pane_current_path}"'

    if-shell "test '#{$TMUX_VERSION_MAJOR} -gt 1 -o \( #{$TMUX_VERSION_MAJOR} -eq 1 -a #{$TMUX_VERSION_MINOR} -ge 8 \)'" 'unbind v; bind v split-window -h -c "#{pane_current_path}"'
    if-shell "test '#{$TMUX_VERSION_MAJOR} -gt 1 -o \( #{$TMUX_VERSION_MAJOR} -eq 1 -a #{$TMUX_VERSION_MINOR} -ge 8 \)'" 'unbind s; bind s split-window -c "#{pane_current_path}"'
  '';

  programs.tmux.historyLimit = 5000;
    # Maximum number of lines held in window history.
    # Type: positive integer, meaning >0
    # Default: 2000
    # Example: 5000
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.keyMode = "vi";
    # VI or Emacs style shortcuts.
    # Type: one of "emacs", "vi"
    # Default: "emacs"
    # Example: "vi"
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.newSession = false;
    # Automatically spawn a session if trying to attach and none are running.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.plugins = (with pkgs; [
    # List of tmux plugins to be included at the end of your tmux configuration. The sensible plugin, however, is defaulted to run at the top of your configuration.
    # Type: list of plugin packages or submodules
    # Default: [ ]
    # Example:
    # with pkgs; [
    #   tmuxPlugins.cpu
    #   {
    #     plugin = tmuxPlugins.resurrect;
    #     extraConfig = "set -g @resurrect-strategy-nvim 'session'";
    #   }
    #   {
    #     plugin = tmuxPlugins.continuum;
    #     extraConfig = ''
    #       set -g @continuum-restore 'on'
    #       set -g @continuum-save-interval '60' # minutes
    #     '';
    #   }
    # ]
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>
  ]) ++ (with pkgs.unstable; [
    tmuxPlugins.nord
  ]);

  # programs.tmux.prefix = "C-a";
    # Set the prefix key. Overrules the "shortcut" option when set.
    # Type: null or string
    # Default: null
    # Example: "C-a"
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.resizeAmount = 5;
    # Number of lines/columns when resizing.
    # Type: positive integer, meaning >0
    # Default: 5
    # Example: 10
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.reverseSplit = false;
    # Reverse the window split shortcuts.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.secureSocket = false;
    # Store tmux socket under /run, which is more secure than /tmp, but as a downside it doesn't survive user logout.
    # Type: boolean
    # Default: true
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.sensibleOnTop = true;
    # Run the sensible plugin at the top of the configuration. It is possible to override the sensible settings using the programs.tmux.extraConfig option.
    # Type: boolean
    # Default: true
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  # programs.tmux.shell = null;
    # Set the default-shell tmux variable.
    # Type: null or string
    # Default: null
    # Example: "\${pkgs.zsh}/bin/zsh"
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  # programs.tmux.shortcut = "a";
    # CTRL following by this key is used as the main shortcut.
    # Type: string
    # Default: "b"
    # Example: "a"
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.terminal = "st-256color";
    # Set the $TERM variable.
    # Type: string
    # Default: "screen"
    # Example: "screen-256color"
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.tmuxinator.enable = true;
    # Whether to enable tmuxinator.
    # Type: boolean
    # Default: false
    # Example: true
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  programs.tmux.tmuxp.enable = false;
    # Whether to enable tmuxp.
    # Type: boolean
    # Default: false
    # Example: true
    # Declared by:
    # <home-manager/modules/programs/tmux.nix>

  home.file.".config/tmuxinator".source = ./tmuxinator;
}
