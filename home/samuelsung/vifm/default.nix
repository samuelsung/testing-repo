{ pkgs, ... }:

let
  vifmimg = pkgs.stdenv.mkDerivation {
    name = "vifmimg";

    src = pkgs.fetchFromGitHub {
      owner = "cirala";
      repo = "vifmimg";
      rev = "afe4f159e1acc9a54dd3a2dc28850f11f97c1ebf";
      sha256 = "11pbfayww7pwyk8a7jgvijx9i96ahyvn5y84gpn0yjd0b9wf75bn";
    };

    dontBuild = true;

    installPhase = ''
      mkdir -p $out/bin

      mv vifmimg $out/bin
      mv vifmrun $out/bin
    '';

    runtimeInputs = [ pkgs.ffmpegthumbnailer ];
  };
in
  {
    home.packages = [ pkgs.vifm vifmimg ];
    home.file.".config/vifm/vifmrc".source = ./vifmrc;
    home.file.".config/vifm/colors".source = ./colors;
    home.file.".local/share/applications/vifm.desktop".source = ./vifm.desktop;
  }
