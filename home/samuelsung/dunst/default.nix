{ pkgs, ... }:

{
  home.packages = with pkgs; [
    libnotify
    dunst
  ];

  home.file.".config/dunst/dunstrc".source = ./dunstrc;
}
