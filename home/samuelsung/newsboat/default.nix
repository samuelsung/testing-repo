{ pkgs, ... }:

{
  home.packages = with pkgs; [
    youtube-dl # downloading youtube
  ];

  programs.newsboat.enable = true;
  # Whether to enable the Newsboat feed reader.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  # programs.newsboat.autoReload
  # Whether to enable automatic reloading while newsboat is running.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  # programs.newsboat.browser
  # External browser to use.
  # Type: string
  # Default: "\${pkgs.xdg_utils}/bin/xdg-open"
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  programs.newsboat.extraConfig = ''
    #show-read-feeds no
    auto-reload yes

    external-url-viewer "urlscan -dc -r 'linkhandler {}'"

    bind-key n down
    bind-key e up
    bind-key n next articlelist
    bind-key e prev articlelist
    bind-key N next-feed articlelist
    bind-key E prev-feed articlelist
    bind-key G end
    bind-key g home
    bind-key d pagedown
    bind-key u pageup
    bind-key i open
    bind-key h quit
    bind-key a toggle-article-read
    bind-key k next-unread
    bind-key K prev-unread
    bind-key D pb-download
    bind-key U show-urls
    bind-key x pb-delete

    color listnormal cyan default
    color listfocus black yellow standout bold
    color listnormal_unread blue default
    color listfocus_unread yellow default bold
    color info red black bold
    color article white default bold

    browser linkhandler
    macro , open-in-browser
    macro t set browser "qndl" ; open-in-browser ; set browser linkhandler
    macro a set browser "tsp youtube-dl --add-metadata -xic -f bestaudio/best" ; open-in-browser ; set browser linkhandler
    macro v set browser "setsid -f mpv" ; open-in-browser ; set browser linkhandler
    macro w set browser "lynx" ; open-in-browser ; set browser linkhandler
    macro d set browser "dmenuhandler" ; open-in-browser ; set browser linkhandler
    macro c set browser "xsel -b <<<" ; open-in-browser ; set browser linkhandler
    macro C set browser "youtube-viewer --comments=%u" ; open-in-browser ; set browser linkhandler
    macro y set browser "youtube-dl-mpv %u" ; open-in-browser ; set browser linkhandler
    macro Y set browser "youtube-dl %u -o - | mpv -" ; open-in-browser ; set browser linkhandler
    macro p set browser "peertubetorrent %u 480" ; open-in-browser ; set browser linkhandler
    macro P set browser "peertubetorrent %u 1080" ; open-in-browser ; set browser linkhandler

    highlight all "---.*---" yellow
    highlight feedlist ".*(0/0))" black
    highlight article "(^Feed:.*|^Title:.*|^Author:.*)" cyan default bold
    highlight article "(^Link:.*|^Date:.*)" default default
    highlight article "https?://[^ ]+" green default
    highlight article "^(Title):.*$" blue default
    highlight article "\\[[0-9][0-9]*\\]" magenta default bold
    highlight article "\\[image\\ [0-9]+\\]" green default bold
    highlight article "\\[embedded flash: [0-9][0-9]*\\]" green default bold
    highlight article ":.*\\(link\\)$" cyan default
    highlight article ":.*\\(image\\)$" blue default
    highlight article ":.*\\(embedded flash\\)$" magenta default
  '';
  # Extra configuration values that will be appended to the end.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  # programs.newsboat.maxItems
  # Maximum number of items per feed, 0 for infinite.
  # Type: signed integer
  # Default: 0
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  # programs.newsboat.queries
  # A list of queries to use.
  # Type: attribute set of strings
  # Default: { }
  # Example: { foo = ''rssurl =~ "example.com"''; }
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  # programs.newsboat.reloadThreads
  # How many threads to use for updating the feeds.
  # Type: signed integer
  # Default: 5
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  # programs.newsboat.reloadTime
  # Time in minutes between reloads.
  # Type: null or signed integer
  # Default: 60
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 

  programs.newsboat.urls = [
    {
      tags = [ "Twitter" "FF14" ];
      url = "https://nitter.net/FF_XIV_JP/rss";
    }

    {
      tags = [ "Twitter" "FF14" ];
      url = "https://nitter.net/FFXIV_NEWS_JP/rss";
    }

    {
      tags = [ "Twitter" "FF14" ];
      url = "https://nitter.net/FF14game8/rss";
    }

    {
      tags = [ "Linux" "Tech" ];
      # title = "Luke Smith";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC2eYFnH61tmytImy1mTYvhA";
    }
    {
      tags = [ "Linux" "Tech" ];
      # title = "DistroTube";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCVls1GmFKf6WlTraIb_IaJg";
    }
    {
      tags = [ "Linux" "Tech" ];
      # title = "Brodie Robertson";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCld68syR8Wi-GY_n4CaoJGA";
    }
    {
      tags = [ "Linux" "Tech" ];
      # title = "The Linux Experiment";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC5UAwBUum7CPN5buc-_N1Fw";
    }
    {
      tags = [ "Linux" "Tech" ];
      # title = "TheFrugalComputerGuy";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCbZ8wD6pmGb9qHqvx9M4YBw";
    }

    {
      tags = [ "Tech" ];
      # title = "Linus Tech Tips";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCXuqSBlHAE6Xw-yeJA0Tunw";
    }
    {
      tags = [ "Tech" ];
      # title = "TechLinked";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCeeFfhMcJa1kjtfZAGskOCA";
    }
    {
      tags = [ "Tech" ];
      # title = "ShortCircuit";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCdBK94H6oZT2Q7l0-b0xmMg";
    }
    {
      tags = [ "Tech" ];
      # title = "Techquickie";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC0vBXGSyV14uvJ4hECDOl0Q";
    }
    {
      tags = [ "Tech" ];
      # title = "Gamers Nexus";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UChIs72whgZI9w6d6FhwGGHA";
    }
    {
      tags = [ "Tech" ];
      # title = "ServeTheHome";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCv6J_jJa8GJqFwQNgNrMuww";
    }
    {
      tags = [ "Tech" ];
      # title = "Level1Techs";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC4w1YQAJMWOz4qtxinq55LQ";
    }
    {
      tags = [ "Tech" "Linux" ];
      # title = "Level1Linux";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCOWcZ6Wicl-1N34H0zZe38w";
    }
    {
      tags = [ "Tech" ];
      # title = "Bitwit";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCftcLVz-jtPXoH3cWUUDwYw";
    }
    {
      tags = [ "Tech" ];
      # title = "Craft Computing";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCp3yVOm6A55nx65STpm3tXQ";
    }
    {
      tags = [ "Tech" ];
      # title = "MobileTechReview";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCW6J17hZ_Vgr6cQgd_kHt5A";
    }
    {
      tags = [ "Tech" ];
      # title = "Greg Salazar";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCmbkRUS_4Efdt5UIhwNqtcw";
    }
    {
      tags = [ "Tech" ];
      # title = "Spaceinvader One";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCZDfnUn74N0WeAPvMqTOrtA";
    }
    {
      tags = [ "Tech" ];
      # title = "Louis Rossmann";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCl2mFZoRqjw_ELax4Yisf6w";
    }
    {
      tags = [ "Tech" ];
      # title = "Taran Van Hemert";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCd0ZD4iCXRXf18p3cA7EQfg";
    }

    {
      tags = [ "Tech" "Coding" ];
      # title = "freeCodeCamp.org";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC8butISFwT-Wl7EV0hUK0BQ";
    }
    {
      tags = [ "Tech" "Coding" ];
      # title = "Kevin Powell";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCJZv4d5rbIKd4QHMPkcABCw";
    }
    {
      tags = [ "Tech" "Coding" ];
      # title = "Code Bullet";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC0e3QhIYukixgh5VVpKHH9Q";
    }
    {
      tags = [ "Tech" "Coding" ];
      # title = "javidx9";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC-yuWVUplUJZvieEligKBkA";
    }
    {
      tags = [ "Tech" "Coding" ];
      # title = "Classsed";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC2-slOJImuSc20Drbf88qvg";
    }

    {
      tags = [ "News" ];
      # title = "城寨 Singjai";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC0zUmHNpkviI6UZ0uqCYrww";
    }
    {
      tags = [ "News" ];
      # title = "堅離地球 · 沈旭暉";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCXf8jlTSP9kp6g4ROCfgvbQ";
    }
    {
      tags = [ "News" ];
      # title = "Gavinchiu趙氏讀書生活";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCmi1257Mo7v4ors9-ekOq1w";
    }
    {
      tags = [ "News" ];
      # title = "RTHK 香港電台";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC6of7UYhctnYmqABjUqzuxw";
    }
    {
      tags = [ "News" ];
      # title = "視點31";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCgo-7AJYXdImA1rIyRkOP1Q";
    }
    {
      tags = [ "News" ];
      # title = "蘋果動新聞 HK Apple Daily";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCeqUUXaM75wrK5Aalo6UorQ";
    }
    {
      tags = [ "News" ];
      # title = "眾新聞";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC7K4DBOzdITZFOjGkea_CCA";
    }
    {
      tags = [ "News" ];
      # title = "有話好說 PTSTalk";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCgxXuXHJ4VwovSTvDiWlzZQ";
    }
    {
      tags = [ "News" ];
      # title = "China Uncensored";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCgFP46yVT-GG4o1TgXn-04Q";
    }
    {
      tags = [ "News" ];
      # title = "America Uncovered";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC_7vFlErTHxVD-IFNB-BFCg";
    }
    {
      tags = [ "News" ];
      # title = "TMHK - Truth Media Hong Kong";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCPJGOTbtle2zjkziXLBkmHA";
    }

    {
      tags = [ "Education" ];
      # title = "Kurzgesagt – In a Nutshell";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCsXVk37bltHxD1rDPwtNM8Q";
    }
    {
      tags = [ "Education" ];
      # title = "ElectroBOOM";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCJ0-OtVpF0wOKEqT2Z1HEtA";
    }
    {
      tags = [ "Education" "Tech" ];
      # title = "Jon Gjengset";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC_iD0xppBwwsrM9DegC5cQQ";
    }
    {
      tags = [ "Education" "Tech" ];
      # title = "Computerphile";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC9-y-6csu5WGm29I7JiwpnA";
    }
    {
      tags = [ "Education" ];
      # title = "志祺七七 X 圖文不符";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCiWXd0nmBjlKROwzMyPV-Nw";
    }
    {
      tags = [ "Education" ];
      # title = "Thomas Frank";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCG-KntY7aVnIGXYEBQvmBAQ";
    }
    {
      tags = [ "Education" ];
      # title = "Keep Productive";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCYyaQsm2HyneP9CsIOdihBw";
    }
    {
      tags = [ "Education" ];
      # title = "蒼藍鴿的醫學天地";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCUn77_F5A65HViL9OEvIpLw";
    }
    {
      tags = [ "Education" ];
      # title = "好青年荼毒室 - 哲學部";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCljo-rxFnc2gl9l8NhhJ66Q";
    }
    {
      tags = [ "Education" ];
      # title = "佑來了";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCWYa0v8bpGyYr0ycqGXifVQ";
    }
    {
      tags = [ "Education" ];
      # title = "Tv Nichigo";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC86IknFsjIuFh8kWHKltuIQ";
    }

    {
      tags = [ "Comedy" ];
      # title = "眼球中央電視台";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCTqPBBnP2T57kmiPQ87986g";
    }
    {
      tags = [ "Comedy" ];
      # title = "Stand up, Brian! 博恩站起來！";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCUGlE8lf5qH--_XlsabI2XQ";
    }
    {
      tags = [ "Comedy" ];
      # title = "喬瑟夫 ChillSeph";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCBY6NwU6OpYQiPYR1urdF0g";
    }
    {
      tags = [ "Comedy" ];
      # title = "賀瓏Hello";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCnXLslDRBPExnUBunhM918Q";
    }
    {
      tags = [ "Comedy" ];
      # title = "酸酸脫口秀";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCNaym1uKpIxdHNpisQP07JQ";
    }
    {
      tags = [ "Comedy" ];
      # title = "囂搞 Shaogao";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCkM5JioKc2MafrXNMNaJCFQ";
    }
    {
      tags = [ "Comedy" "Education" ];
      # title = "Taiwan Bar";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCRNsHFT7BFoAPBcuAa5sgEQ";
    }
    {
      tags = [ "Comedy" ];
      # title = "Channel Super Fun";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCBZiUUYeLfS5rIj4TQvgSvA";
    }
    {
      tags = [ "Comedy" ];
      # title = "LinusCatTips";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCL81YHgzH8tcrFfOJJwlSQw";
    }
    {
      tags = [ "Comedy" ];
      # title = "杜汶澤喱騷 Chapman To's Lateshow";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCU5MOgUdLqitxKZRZy9vnnw";
    }
    {
      tags = [ "Comedy" ];
      # title = "テイコウペンギン";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCUTgXNqRBqR33D5q10DfQXw";
    }
    {
      tags = [ "Comedy" ];
      # title = "全力回避フラグちゃん!";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCo_nZN5yB0rmfoPBVjYRMmw";
    }
    {
      tags = [ "Comedy" ];
      # title = "吉田製作所";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC9WJo5ZJVXMZiA5XV2jLx5Q";
    }
    {
      tags = [ "Comedy" ];
      # title = "吉田研究所【事実上メインチャンネル】";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCLE2PTNDt--NLGLVbfOfHkQ";
    }

    {
      tags = [ "Music" ];
      # title = "GALNERYUS";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCzM-_tDgHG8KCUGByYEcrZA";
    }
    {
      tags = [ "Music" ];
      # title = "BUMP OF CHICKEN";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCOfESRUR5duQ2hMnTQ4oqhA";
    }
    {
      tags = [ "Music" ];
      # title = "ウォルビスカーター";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCrFREdqSkLslvG2BpYpZHmg";
    }
    {
      tags = [ "Music" ];
      # title = "高登音樂台 HKGolden Music";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC-PbFMH4uhmqO71Gyxr5A-g";
    }

    {
      tags = [ "Gaming" ];
      # title = "PewDiePie";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC-lHJZR3Gqxm24_Vd_AJ5Yw";
    }
    {
      tags = [ "Gaming" "minecraft" ];
      # title = "Pixlriffs";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCgGjBqZZtAjxfpGSba7d6ww";
    }

    {
      tags = [ "Gaming" "FF14" ];
      # title = "FINAL FANTASY XIV";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCpx2BZg8ABgaDV50sGJtWAg";
    }
    {
      tags = [ "Gaming" "FF14" ];
      # title = "根暗ちゃんねる";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCeX8xf5ucBH3YKRScWL8i8g";
    }
    {
      tags = [ "Gaming" "FF14" ];
      # title = "むにむに";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCN9uUYJgxAViDEhB0UOoJ4g";
    }
    {
      tags = [ "Gaming" "FF14" ];
      # title = "FF14_じごちゃんねる";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCloiZukrArW5wcXdyT_GlYQ";
    }
    {
      tags = [ "Gaming" "FF14" ];
      # title = "鯛茶アルテミス";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCztZaUtpXAp2rhAvLbvXbkg";
    }
    {
      tags = [ "Gaming" "FF14" ];
      # title = "Asmongold TV";
      url = "https://www.youtube.com/feeds/videos.xml?channel_id=UCQeRaTukNYft1_6AZPACnog";
    }
  ];

  # List of news feeds.
  # Type: list of submodules
  # Default: [ ]
  # Example: [ { tags = [ "foo" "bar" ] ; url = "http://example.com"; } ]
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  # programs.newsboat.urls.*.tags
  # Feed tags.
  # Type: list of strings
  # Default: [ ]
  # Example: [ "foo" "bar" ]
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  # programs.newsboat.urls.*.title
  # Feed title.
  # Type: null or string
  # Default: null
  # Example: "ORF News"
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 
  # programs.newsboat.urls.*.url
  # Feed URL.
  # Type: string
  # Example: "http://example.com"
  # Declared by:
  # <home-manager/modules/programs/newsboat.nix> 


  #   user.service.newsboat-timer = {
  #     Unit = {
  #       Description = "Newsboat reload timer.";
  #     };

  #     Service = {}
  #   };

  #   user.timer.newsboat-timer = {
  #     Unit = {
  #       Description = "Newsboat reload timer.";
  #     };
  #   };
}
