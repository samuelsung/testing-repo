local constants = require('constants');
local nord = constants.nord;

-- Auto Indent
vim.bo.autoindent = true;
vim.bo.smartindent = true;
vim.wo.wrap = true;
vim.bo.cindent = true;

-- Use Spaces instead of Tabs
vim.bo.expandtab = true;
vim.o.expandtab = true;

-- Enable Smarttab
vim.o.smarttab = true;

-- Set the tab to 4 spaces
vim.bo.shiftwidth = 2;
vim.o.shiftwidth = 2;
vim.bo.tabstop = 2;
vim.o.tabstop = 2;

-- Splitting
vim.o.splitbelow = true;
vim.o.splitright = true;
 
-- Set the command prompt to the bottom line
vim.o.cmdheight = 1;

-- Left 10 lines at the end
vim.o.scrolloff = 10

-- always show signcolumns
vim.wo.signcolumn = 'yes';

-- " Folding
-- set foldmethod=syntax
-- "set foldcolumn=1
-- set foldlevelstart=99

-- List Characters
vim.wo.list = true;
vim.wo.listchars = "eol:¬,tab:▸ ,trail:.";

-- Enable Counter
vim.o.ruler = true;

-- Enable the number column
vim.wo.number = true;
vim.wo.relativenumber = true;

-- Enable syntax highlighting
vim.cmd('syntax on');
vim.cmd('syntax enable');

-- do not show mode
vim.o.showmode = false;

-- Enable 256 colors palette
vim.go.t_Co = '256';
-- " let g:indentLine_color_gui = '#4c566a'
-- " let g:indentLine_bgcolor_gui = '#FF5F00'

-- Enable Dark Mode
vim.o.background = 'dark';

-- Use Truecolorscheme
vim.o.termguicolors = true;

-- Set Indent Line Color
vim.g.indentLine_color_gui = '#4c566a';
vim.g.nord_cursor_line_number_background = true;

-- force nord to load tree sitter colors first
vim.g.loaded_nvim_treesitter = 1;

-- Enable Color Scheme
vim.cmd('colorscheme nord');

-- so that treesitter can be load later
vim.g.loaded_nvim_treesitter = undefined;

-- Disable Background for tranparency
vim.cmd('highlight Normal ctermbg=NONE guibg=NONE');
vim.cmd('highlight LineNr ctermbg=NONE guibg=NONE');
vim.cmd('highlight SignColumn ctermbg=NONE guibg=NONE');
vim.cmd('highlight VertSplit ctermbg=NONE guibg=NONE');


wrap = function(fn, default_value)
  return function()
    local str = fn();
    return (str == '' or str == null) and default_value or str;
  end;
end;

filter = function(fn, sub_str)
  return function()
    return fn():gsub(sub_str, '');
  end;
end;

to_lower = function(fn)
  return function()
    return string.lower(fn());
  end;
end;

concat_fn = function(fn1, fn2) 
  return function()
    return fn1(fn2())
  end
end

-- Galaxy Line Config
local gl = require('galaxyline');
local glc = require('galaxyline.condition');
local glvcs = require('galaxyline.provider_vcs');
local glfileinfo = require('galaxyline.provider_fileinfo');
local glbuffer = require('galaxyline.provider_buffer');
local gllsp = require('galaxyline.provider_lsp');

gl.section.left = {
  {
    File = {
      provider = function()
        -- n	    Normal
        -- no	    Operator-pending
        -- nov	    Operator-pending (forced charwise |o_v|)
        -- noV	    Operator-pending (forced linewise |o_V|)
        -- noCTRL-V Operator-pending (forced blockwise |o_CTRL-V|)
        -- CTRL-V is one characte
        -- niI	    Normal using |i_CTRL-O| in |Insert-mode|
        -- niR	    Normal using |i_CTRL-O| in |Replace-mode|
        -- niV	    Normal using |i_CTRL-O| in |Virtual-Replace-mode|
        -- v	    Visual by character
        -- V	    Visual by line
        -- CTRL-V   Visual blockwise
        -- s	    Select by character
        -- S	    Select by line
        -- CTRL-S   Select blockwise
        -- i	    Insert
        -- ic	    Insert mode completion |compl-generic|
        -- ix	    Insert mode |i_CTRL-X| completion
        -- R	    Replace |R|
        -- Rc	    Replace mode completion |compl-generic|
        -- Rv	    Virtual Replace |gR|
        -- Rx	    Replace mode |i_CTRL-X| completion
        -- c	    Command-line editing
        -- cv	    Vim Ex mode |gQ|
        -- ce	    Normal Ex mode |Q|
        -- r	    Hit-enter prompt
        -- rm	    The -- more -- prompt
        -- r?	    |:confirm| query of some sort
        -- !	    Shell or external command is executing
        -- t	    Terminal mode: keys go to the job
        local alias = {
          n = 'NORMAL',
          i = 'INSERT',
          c = 'COMMAND',
          v = 'VISUAL',
          V = 'V-LINE',
          R = 'REPLACE',
          t = 'TERMINAL',
          [''] = 'V-BLOCK',
        };

        local fgcolors = {
          n = nord[6],
          i = nord[0],
          c = nord[0],
          v = nord[0],
          V = nord[0],
          R = nord[1],
          t = nord[0],
          [''] = nord[0],
        }

        local bgcolors = {
          n = nord[2],
          i = nord[10],
          c = nord[12],
          v = nord[15],
          V = nord[15],
          R = nord[11],
          t = nord[10],
          [''] = nord[15],
        }

        if (fgcolors[vim.fn.mode()] and bgcolors[vim.fn.mode()]) then
          vim.api.nvim_command('hi GalaxyFile guifg=' .. fgcolors[vim.fn.mode()] .. ' guibg=' .. bgcolors[vim.fn.mode()])
          vim.api.nvim_command('hi FileSeparator guifg=' .. fgcolors[vim.fn.mode()] .. ' guibg=' .. bgcolors[vim.fn.mode()])
        end;
        return wrap(glfileinfo.get_file_icon, '')()
          .. wrap(glbuffer.get_buffer_number, '')()
          .. ':'
          .. wrap(glfileinfo.get_current_file_name, ' ')()
          .. '('
          .. filter(glfileinfo.line_column, ' ')()
          .. ') ';
      end,
      highlight = { constants.nord[12], constants.nord[3] },
    },
  },
  {
    GitBranch = {
      provider = concat_fn(function(str) return str and ('   ' .. str) or '' end, glvcs.get_git_branch),
      condition = function() return glc.check_git_workspace() and vim.fn.winwidth(0) > 60 end,
      highlight = { constants.nord[15], constants.nord[2] },
      separator = ' ',
      separator_highlight = { constants.nord[2], constants.nord[2] },
    },
  },
  {
    DiffAdd = {
      provider = wrap(glvcs.diff_add, '0 '),
      condition = glc.check_git_workspace,
      highlight = { constants.nord[14], constants.nord[2] },
      icon = '+',
    },
  },
  {
    DiffModified = {
      provider = wrap(glvcs.diff_modified, '0 '),
      condition = glc.check_git_workspace,
      highlight = { constants.nord[13], constants.nord[2] },
      icon = ' ~',
    },
  },
  {
    DiffRemove = {
      provider = wrap(glvcs.diff_remove, '0 '),
      condition = glc.check_git_workspace,
      highlight = { constants.nord[11], constants.nord[2] },
      icon = ' -',
    },
  },
  {
    Sep = {
      provider = function() return '' end,
      separator = ' ',
      separator_highlight = { constants.nord[1], constants.nord[1] },
    },
  },
};

gl.section.right = {
  {
    Lsp = {
      provider = function() return gllsp.get_lsp_client() .. ' '; end,
      condition = function() return vim.fn.winwidth(0) > 60 end,
    },
  },
  {
    FixOnSave = {
      provider = function()
        return vim.g.auto_format_state and '(fmt: on)' or '(fmt: off)'
      end,
      --condition = function() return vim.fn.winwidth(0) > 50 end,
    },
  },
};

