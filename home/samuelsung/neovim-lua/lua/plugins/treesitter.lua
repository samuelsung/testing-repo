local commentstring_config = {
  nix = '# %s',
  sql = '-- %s',
  mysql = '-- %s',
};

-- -- filetype indent plugin on | syn on
require('nvim-treesitter.configs').setup({
  ensure_installed = 'all',
  highlight = {
    enable = true,
  },
  -- incremental_selection keybinds is in navigations
  indent = {
    enable = true,
  },

  autotag = {
    enable = true,
  },

  --refactor = {
    --navigation = {
      --enable = true,

      --keymaps = {
        ----goto_definition = "gnd",
        ----goto_definition_lsp_fallback = 'gnd',
        --list_definitions = "gnD",
        --list_definitions_toc = "gO",
        --goto_next_usage = "<a-*>",
        --goto_previous_usage = "<a-#>",
      --},
    --},
  --},
  context_commentstring = {
    enable = true,
    enable_autocmd = false,

    config = commentstring_config,
  },
});

_G.on_commentary = function(str)
  local is_success, rtn = pcall(_G.context_commentstring.update_commentstring_and_run, str);

  if (is_success) then
    return rtn;
  end

  -- failback to filetype if the language is not supported by tree-sitter
  local filetype = vim.bo.filetype;
  local commentstring = commentstring_config[filetype];
  if (commentstring ~= nil) then
    vim.api.nvim_buf_set_option(0, 'commentstring', commentstring);
  end;

  return vim.api.nvim_replace_termcodes('<Plug>' .. str, true, true, true);
end;

