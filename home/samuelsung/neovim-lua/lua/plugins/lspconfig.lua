local lspconfig = require('lspconfig');

lspconfig.tsserver.setup({
  cmd = { 'typescript-language-server', '--stdio' },
  filetypes = { 'javascript', 'javascriptreact',
    'javascript.jsx',
    'typescript',
    'typescriptreact',
    'typescript.tsx',
  },
  root_dir = require('lspconfig/util').root_pattern(
    'package.json',
    'tsconfig.json',
    'jsconfig.json',
    '.git'
  ),
  settings = { documentFormatting = false },
});

lspconfig.cssls.setup({
  cmd = { "vscode-css-language-server", "--stdio" },
});

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.resolveSupport = {
  properties = {
    'documentation',
    'detail',
    'additionalTextEdits',
  }
}

lspconfig.rls.setup({
  capabilities = capabilities
});

local eslint = {
  lintCommand = 'eslint_d -f visualstudio --stdin --stdin-filename ${INPUT}',
  lintStdin = true,
  lintFormats = {
    '%f(%l,%c): %tarning %m',
    '%f(%l,%c): %rror %m',
  },
  lintIgnoreExitCode = true,
  --formatCommand = 'eslint_d --fix-to-stdout --stdin --stdin-filename ${INPUT}',
  --formatStdin = true
}

local shellcheck = {
  lintCommand = 'shellcheck -f gcc -x',
  lintSource = 'shellcheck',
  lintFormats = {
    '%f:%l:%c: %trror: %m',
    '%f:%l:%c: %tarning: %m',
    '%f:%l:%c: %tote: %m',
  },
}

local sqlfluff = {
  lintCommand = 'sqlfluff lint - | sed \'s/[LP]:\\s*//g\'',
  lintStdin = true,
  lintFormats = {
    '%l | %c | %m',
  },
  lintIgnoreExitCode = true,
}

lspconfig.efm.setup({
  init_options = { documentFormatting = true },
  root_dir = function(fname)
    return require('lspconfig/util').root_pattern(
    '.eslintrc.js',
    '.eslintrc.json',
    '.git'
    )(fname) or vim.loop.os_homedir();
  end,
  filetypes = {
    'javascript',
    'javascriptreact',
    'javascript.jsx',
    'typescript',
    'typescript.tsx',
    'typescriptreact',
    'sh'
    -- 'sql',
    -- 'mysql',
  },
  settings = {
    languages = {
      javascript = {eslint},
      javascriptreact = {eslint},
      ['javascript.jsx'] = {eslint},
      typescript = {eslint},
      ['typescript.tsx'] = {eslint},
      typescriptreact = {eslint},
      -- sql = {sqlfluff},
      -- mysql = {sqlfluff},
      sh = {shellcheck},
    },
  },
});

lspconfig.texlab.setup({
  settings = {
    bibtex = {
      formatting = {
        lineLength = 120
      }
    },
    latex = {
      build = {
        args = { "-pdf", "-interaction=nonstopmode", "-synctex=1", "%f" },
        executable = "latexmk",
        onSave = false
      },
      forwardSearch = {
        args = {},
        onSave = false
      },
      lint = {
        onChange = true
      }
    }
  }
});

lspconfig.rnix.setup({});

lspconfig.hls.setup({});

vim.cmd([[
  augroup lsp
    au!
    au FileType java lua _G.attach_jdtls()
  augroup end
]])

_G.attach_jdtls = function()
  require('jdtls').start_or_attach({
    -- on_attach = on_attach,
    cmd = { 'jdt-language-server' },
    root_dir = require('jdtls.setup').find_root({'build.gradle', 'pom.xml', '.git'}),
    -- init_options = {bundles = bundles}
  });
end;
