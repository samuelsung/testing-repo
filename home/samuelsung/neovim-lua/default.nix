{ pkgs, ... }:

{
  home.packages = (with pkgs; [
    gcc # tree-sitter needs it
    rnix-lsp
    nixpkgs-fmt
    shellcheck
    shfmt
  ]) ++ (with pkgs.unstable; [
    efm-langserver
    tree-sitter
    nodePackages.typescript-language-server
    nodePackages.typescript
    nodePackages.eslint_d
    nodePackages.eslint
    texlab
  ]) ++ (with pkgs.custom; [
    jdtls
    nodePackages.sql-language-server
    nodePackages.vscode-langservers-extracted
    nodePackages.sql-lint
    python3Packages.sqlfluff
  ]);

  ### Basics
  programs.neovim.enable = true;
  # Whether to enable Neovim.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  programs.neovim.package = pkgs.unstable.neovim-unwrapped;
  # The package to use for the neovim binary.
  # Type: package
  # Default: pkgs.neovim-unwrapped
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  ### Alias
  programs.neovim.viAlias = true;
  # Symlink vi to nvim binary.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>
  programs.neovim.vimAlias = true;
  # Symlink vim to nvim binary.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>
  programs.neovim.vimdiffAlias = true;
  # Alias vimdiff to nvim -d.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  ### Providers
  programs.neovim.withNodeJs = true;
  # Enable node provider. Set to true to use Node plugins.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  programs.neovim.withPython3 = true;
  # Enable Python 3 provider. Set to true to use Python 3 plugins.
  # Type: boolean
  # Default: true
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>
  programs.neovim.withRuby = true;
  # Enable ruby provider.
  # Type: null or boolean
  # Default: true
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  # Plugins
  programs.neovim.plugins =
    # List of vim plugins to install optionally associated with configuration to be placed in init.vim.
    # This option is mutually exclusive with configure.
    # Type: list of package or submodules
    # Default: [ ]
    # Example:
    # with pkgs.vimPlugins; [
    #   yankring
    #   vim-nix
    #   { plugin = vim-startify;
    #     config = "let g:startify_change_to_vcs_root = 0";
    #   }
    # ]
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>
    (with pkgs.vimPlugins; [
      # Files
      bclose-vim

      # Indents
      indentLine

      # Navigations
      vim-table-mode
      vim-easymotion
    # ]) ++ (with pkgs.unstable.vimPlugins; [ # TODO
      # Commenting
      vim-commentary

      # Formatting
      nvim-autopairs
      nvim-treesitter-refactor

      # Git
      gitsigns-nvim

      # LSP
      nvim-compe
      nvim-jdtls
      nvim-lspconfig

      # Themes
      galaxyline-nvim
      nvim-treesitter
      nvim-web-devicons
      nord-vim

      # Navigations
      plenary-nvim
      popup-nvim
      telescope-nvim
    ]) ++ (with pkgs.custom.vimPlugins; [
      # Commenting
      nvim-ts-context-commentstring

      # Formatting
      formatter-nvim
      nvim-ts-autotag

      # Files
      vifm-vim
    ]);

  # programs.neovim.extraPackages = [];
  # Extra packages available to nvim.
  # Type: list of packages
  # Default: [ ]
  # Example: "[ pkgs.shfmt ]"
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  # programs.neovim.extraPython3Packages = []; # deprecated
  # A function in python.withPackages format, which returns a list of Python 3 packages required for your plugins to work.
  # Type: python3 packages in python.withPackages format or list of packages
  # Default: "ps: []"
  # Example:
  # (ps: with ps; [ python-language-server ])
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  # programs.neovim.extraPythonPackages = []; # deprecated
  # A function in python.withPackages format, which returns a list of Python 2 packages required for your plugins to work.
  # Type: python packages in python.withPackages format or list of packages
  # Default: "ps: []"
  # Example:
  # (ps: with ps; [ pandas jedi ])
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  # Configurations
  # programs.neovim.configure = {};
  # Generate your init file from your list of plugins and custom commands, and loads it from the store via nvim -u /nix/store/hash-vimrc
  # This option is mutually exclusive with extraConfig and plugins.
  # Type: attribute set
  # Default: { }
  # Example:
  # configure = {
  #     customRC = $'''
  #     " here your custom configuration goes!
  #     $''';
  #     packages.myVimPackage = with pkgs.vimPlugins; {
  #       # loaded on launch
  #       start = [ fugitive ];
  #       # manually loadable by calling `:packadd $plugin-name`
  #       opt = [ ];
  #     };
  #   };
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  # home.file.".config/nvim/coc-settings.json".source = ./coc-settings.json;
  # Coc configuration file

  programs.neovim.extraConfig = ''
    lua require('init')
    "##### auto fcitx  ###########
    let g:input_toggle = 0
    function! Fcitx2en()
       let s:input_status = system("fcitx5-remote")
       if s:input_status == 2
          let g:input_toggle = 1
          let l:a = system("fcitx5-remote -c")
       endif
    endfunction

    function! Fcitx2zh()
       let s:input_status = system("fcitx5-remote")
       if s:input_status != 2 && g:input_toggle == 1
          let l:a = system("fcitx5-remote -o")
          let g:input_toggle = 0
       endif
    endfunction

    set ttimeoutlen=50
    "Exit insert mode
    autocmd InsertLeave * call Fcitx2en()
    "Enter insert mode
    autocmd InsertEnter * call Fcitx2zh()
    "##### auto fcitx end ######
  '';
  # Custom vimrc lines.
  # This option is mutually exclusive with configure.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Example:
  # ''
  # set nocompatible
  # set nobackup
  # ''
  # Declared by:
  # <home-manager/modules/programs/neovim.nix>

  home.file.".config/nvim/lua".source = ./lua;
}
