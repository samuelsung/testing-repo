{ aspect-ratio ? "16:9"
, merge-strategies ? "theirs"
}:

{ pkgs, lib, ... }:

{
  home.packages = with pkgs; [
    # there seems to have package collision
    # see https://github.com/nix-community/home-manager/issues/2121
    (lib.lowPrio final-fantasy-xiv-sync)
  ];

  # Steam command for running ffxiv
  # NOSTEAM=1 VK_ICD_FILENAMES=/run/opengl-driver/share/vulkan/icd.d/nvidia_icd.json:/run/opengl-driver-32/share/vulkan/icd.d/nvidia_icd.json:/run/opengl-driver-32/share/vulkan/icd.d/intel_icd.i686.json PROTON_LOG=1 __NV_PRIME_RENDER_OFFLOAD=1 __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0 __GLX_VENDOR_LIBRARY_NAME=nvidia __VK_LAYER_NV_optimus=NVIDIA_only %command%
  home.file.".config/final-fantasy-xiv-sync/config.conf" = {
    force = true;
    text = ''
      branch=master
      ratio=${aspect-ratio}
      merge_strategies=${merge-strategies}
      character_id=FFXIV_CHR004000000272108B # Aegis Samuel Sung
      character_id=FFXIV_CHR0040000002896549 # Aegis Samuel Ssung
      character_id=FFXIV_CHR004000000289655E # Aegis Samuel Sssung
      character_id=FFXIV_CHR00400000028A14DD # Aegis Samuel Ssssung
      character_id=FFXIV_CHR00400000028BA7E1 # Aegis Samuel Sssssung
      character_id=FFXIV_CHR00400000028BA821 # Aegis Samuel Ssusssung
      character_id=FFXIV_CHR00400000028BA861 # Aegis Samuel Sssssssung
      character_id=FFXIV_CHR00400000028BA87E # Aegis Samuel Ssssssssung
      character_id=FFXIV_CHR00400000029300AB # Aegis Ssamuel Sung
      character_id=FFXIV_CHR00400000027210D4 # Aegis See Chung
      character_id=FFXIV_CHR004000000290FDD5 # Garuda Miya Mizuki

      source_dir=/home/samuelsung/.final-fantasy-xiv-config/
      target_dir=/home/samuelsung/.steam/root/steamapps/compatdata/312060/pfx/drive_c/users/steamuser/Documents/My Games/FINAL FANTASY XIV - A Realm Reborn/
    '';
  };
}
