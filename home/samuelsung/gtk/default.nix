{ pkgs, ... }:

let
  oreo-nord-cursors = pkgs.stdenv.mkDerivation {
    name = "oreo-nord-cursors-git";

    src = pkgs.fetchFromGitHub {
      owner = "0jdxt";
      repo = "oreo-nord-cursors";
      rev = "314d4ebbcfb8cd10daad3b90f7af92359c5660e1";
      sha256 = "SpEma9W7pBdIx5hU6ArbOdcQ3a3RLROZzvA/qGbK76E=";
    };

    nativeBuildInputs = with pkgs; [
      gtk-engine-murrine
      inkscape
      xorg.xcursorgen
    ];

    prePatch = ''sed -i "s@/usr@$out@" Makefile'';

    buildPhase = "make";

    meta = with pkgs.lib; {
      homepage = "https://github.com/0jdxt/oreo-nord-cursors";
      description = "Fork of oreo-cursors with Nord colors";
      license = licenses.gpl2;
      maintainers = [ ];
      platforms = platforms.linux;
    };
  };
in
{
  home.packages = [ oreo-nord-cursors ];

  gtk.enable = true;
  # Whether to enable GTK 2/3 configuration.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.font.package = pkgs.noto-fonts-cjk;
  # Package providing the font. This package will be installed to your profile. If null then the font is assumed to already be available in your profile.
  # Type: null or package
  # Default: null
  # Example:
  # pkgs.dejavu_fonts
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.font.name = "Noto Sans CJK JP 10";
  # The family name and size of the font within the package.
  # Type: string
  # Example: "DejaVu Sans 8"
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.gtk2.extraConfig = ''
    gtk-cursor-theme-name="oreo_nord_snow_1_cursors"
    gtk-cursor-theme-size=0
    gtk-toolbar-style=GTK_TOOLBAR_BOTH
    gtk-toolbar-icon-size=GTK_ICON_SIZE_BUTTON
    gtk-button-images=1
    gtk-menu-images=1
    gtk-enable-event-sounds=1
    gtk-enable-input-feedback-sounds=1
    gtk-xft-antialias=1
    gtk-xft-hinting=1
    gtk-xft-hintstyle="hintfull"
    gtk-xft-rgba="rgb"
  '';
  # Extra configuration lines to add verbatim to ~/.gtkrc-2.0.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Example: "gtk-can-change-accels = 1"
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.gtk3.bookmarks = [
    "file:///home/samuelsung/nextcloud"
    "smb://freenas.local/main_set/main_set main_set on freenas.local"
  ];
  # Bookmarks in the sidebar of the GTK file browser
  # Type: list of strings
  # Default: [ ]
  # Example: [ "file:///home/jane/Documents" ]
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.gtk3.extraConfig = {
    gtk-cursor-theme-name = "oreo_nord_snow_1_cursors";
    gtk-cursor-theme-size = 0;
    gtk-toolbar-style = "GTK_TOOLBAR_BOTH";
    gtk-toolbar-icon-size = "GTK_ICON_SIZE_BUTTON";
    gtk-button-images = 1;
    gtk-menu-images = 1;
    gtk-enable-event-sounds = 1;
    gtk-enable-input-feedback-sounds = 1;
    gtk-xft-antialias = 1;
    gtk-xft-hinting = 1;
    gtk-xft-hintstyle = "hintfull";
    gtk-xft-rgba = "rgb";
  };
  # Extra configuration options to add to ~/.config/gtk-3.0/settings.ini.
  # Type: attribute set of boolean or signed integer or strings
  # Default: { }
  # Example: { gtk-cursor-blink = false; gtk-recent-files-limit = 20; }
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.gtk3.extraCss = "";
  # Extra configuration lines to add verbatim to ~/.config/gtk-3.0/gtk.css.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.iconTheme.package = pkgs.flat-remix-icon-theme;
  # Package providing the theme. This package will be installed to your profile. If null then the theme is assumed to already be available in your profile.
  # Type: null or package
  # Default: null
  # Example:
  # pkgs.gnome3.gnome_themes_standard
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.iconTheme.name = "Flat-Remix-Blue-Dark";
  # The name of the theme within the package.
  # Type: string
  # Example: "Adwaita"
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.theme.package = pkgs.nordic;
  # Package providing the theme. This package will be installed to your profile. If null then the theme is assumed to already be available in your profile.
  # Type: null or package
  # Default: null
  # Example:
  # pkgs.gnome3.gnome_themes_standard
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>

  gtk.theme.name = "Nordic";
  # The name of the theme within the package.
  # Type: string
  # Example: "Adwaita"
  # Declared by:
  # <home-manager/modules/misc/gtk.nix>
}
