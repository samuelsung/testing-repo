{ config, pkgs, ... }:

{
  home.sessionVariables = {
    EDITOR = "nvim";
    TERMINAL = "st";
  };

  home.sessionPath = [
    "$HOME/.local/bin"
  ];

  home.packages = with pkgs; [
    unstable.lab
    ripgrep
    fzf
    exa
  ];

  programs.zsh.enable = true;
  # Whether to enable Z shell (Zsh).
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.enableAutosuggestions = true;
  # Enable zsh autosuggestions
  # Type: unspecified
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.enableCompletion = false;
  # Enable zsh completion. Don't forget to add
  #   environment.pathsToLink = [ "/share/zsh" ];
  # to your system configuration to get completion for system packages (e.g. systemd).
  # Type: boolean
  # Default: true
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.enableVteIntegration = false;
  # Whether to enable integration with terminals using the VTE library. This will let the terminal track the current working directory.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/misc/vte.nix> 

  programs.zsh.autocd = true;
  # Automatically enter into a directory if typed directly into shell.
  # Type: null or boolean
  # Default: null
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.cdpath = [ ];
  # List of paths to autocomplete calls to `cd`.
  # Type: list of strings
  # Default: [ ]
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.defaultKeymap = "viins";
  # The default base keymap to use.
  # Type: null or one of "emacs", "vicmd", "viins"
  # Default: null
  # Example: "emacs"
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.dotDir = ".config/zsh";
  # Directory where the zsh configuration and more should be located, relative to the users home directory. The default is the home directory.
  # Type: null or string
  # Default: null
  # Example: ".config/zsh"
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.envExtra = "";
  # Extra commands that should be added to .zshenv.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.history.expireDuplicatesFirst = false;
  # Expire duplicates first.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.history.extended = true;
  # Save timestamp into the history file.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.history.ignoreDups = false;
  # Do not enter command lines into the history list if they are duplicates of the previous event.
  # Type: boolean
  # Default: true
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.history.ignoreSpace = false;
  # Do not enter command lines into the history list if the first character is a space.
  # Type: boolean
  # Default: true
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.history.path = "${config.xdg.dataHome}/zsh/zsh_history";
  # History file location
  # Type: string
  # Default: ".zsh_history"
  # Example:
  # "${config.xdg.dataHome}/zsh/zsh_history"
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.history.save = 100000000;
  # Number of history lines to save.
  # Type: signed integer
  # Default: 10000
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.history.share = true;
  # Share command history between zsh sessions.
  # Type: boolean
  # Default: true
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.history.size = 100000000;
  # Number of history lines to keep.
  # Type: signed integer
  # Default: 10000
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.initExtra =
    # Extra commands that should be added to .zshrc.
    # Type: strings concatenated with "\n"
    # Default: ""
    # Declared by:
    # <home-manager/modules/programs/zsh.nix> 

    # Plugins
    (with pkgs; ''
      source ${zsh-fast-syntax-highlighting}/share/zsh/site-functions/fast-syntax-highlighting.plugin.zsh
      source ${zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme
      source ${zsh-you-should-use}/share/zsh/plugins/you-should-use/you-should-use.plugin.zsh
      source ${unstable.lab}/share/zsh/site-functions/_lab
    '') + ''
      zstyle ':completion:*' menu select
      # Auto complete with case insenstivity
      zstyle ':completion:*' matcher-list ${"''"} 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

      # Include hidden files in autocomplete:
      _comp_options+=(globdots)

      zmodload zsh/complist

      # Edit line in nvim
      autoload edit-command-line; zle -N edit-command-line
      bindkey -M vicmd '^v' edit-command-line

      ## menuselect
      ### Use vim keys in tab complete menu:
      bindkey -M menuselect 'h' vi-backward-char
      bindkey -M menuselect 'e' vi-up-line-or-history
      bindkey -M menuselect 'i' vi-forward-char
      bindkey -M menuselect 'n' vi-down-line-or-history

      ## Auto Suggestion
      bindkey -v '^?' backward-delete-char
      bindkey '^ ' autosuggest-accept

      ## vim mode
      ### I use colemak 🙂
      bindkey -M vicmd 'h' vi-backward-char
      bindkey -M vicmd 'e' vi-up-line-or-history
      bindkey -M vicmd 'i' vi-forward-char
      bindkey -M vicmd 'n' vi-down-line-or-history
      bindkey -M vicmd 'u' vi-insert
      bindkey -M vicmd 'U' vi-insert-bol
      bindkey -M vicmd 'l' undo
      bindkey -M vicmd 'L' redo
      bindkey -M vicmd 'k' vi-repeat-search
      bindkey -M vicmd 'K' vi-rev-repeat-search
      bindkey -M visual 'n' down-line
      bindkey -M visual 'e' up-line
      bindkey -M visual 'h' vi-backward-char
      bindkey -M visual 'i' vi-forward-char
      bindkey -M viopp 'n' down-line
      bindkey -M viopp 'e' up-line
      bindkey -M viopp 'h' vi-backward-char
      bindkey -M viopp 'i' vi-forward-char

      ### Change cursor shape for different vi modes.
      function zle-keymap-select {
        if [[ ${"$"}{KEYMAP} == vicmd ]] ||
           [[ $1 = 'block' ]]; then
          echo -ne '\e[1 q'
    
        elif [[ ${"$"}{KEYMAP} == main ]] ||
             [[ ${"$"}{KEYMAP} == viins ]] ||
             [[ ${"$"}{KEYMAP} = ${"''"} ]] ||
             [[ $1 = 'beam' ]]; then
          echo -ne '\e[5 q'
        fi
      }
      zle -N zle-keymap-select

      ### ci", ci', ci`, di", etc
      autoload -U select-quoted
      zle -N select-quoted
      for m in visual viopp; do
        for c in {a,i}{\',\",\`}; do
          bindkey -M $m $c select-quoted
        done
      done
    
      ### ci{, ci(, ci<, di{, etc
      autoload -U select-bracketed
      zle -N select-bracketed
      for m in visual viopp; do
        for c in {a,i}${"$"}{(s..)^:-'()[]{}<>bB'}; do
          bindkey -M $m $c select-bracketed
        done
      done

      zle-line-init() {
          zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
          echo -ne "\e[5 q"
      }
      zle -N zle-line-init
    
      # Use beam shape cursor on startup.
      echo -ne '\e[5 q'
      # Use beam shape cursor for each new prompt.
      preexec() { echo -ne '\e[5 q' ;}
    
      # Use lf to switch directories and bind it to ctrl-o
      lfcd () {
          tmp="$(mktemp)"
          lf -last-dir-path="$tmp" "$@"
          if [ -f "$tmp" ]; then
              dir="$(cat "$tmp")"
              rm -f "$tmp"
              if [ -d "$dir" ]; then
                  if [ "$dir" != "$(pwd)" ]; then
                      cd "$dir"
                  fi
              fi
          fi
      }

      bindkey -s '^o' 'lfcd\n'

      # p10k config
      source ${./p10k.zsh}
    '';

  programs.zsh.initExtraBeforeCompInit = ''
    PF_INFO="ascii title os host kernel wm shell editor uptime memory palette" ${pkgs.dash}/bin/dash ${pkgs.pfetch}/bin/pfetch

    # direnv
    (( ${"$"}{+commands[direnv]} )) && emulate zsh -c "$(direnv export zsh)"

    # Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
    # Initialization code that may require console input (password prompts, [y/n]
    # confirmations, etc.) must go above this block; everything else may go below.
    if [[ -r "${"$"}{XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${"$"}{(%):-%n}.zsh" ]]; then
      source "${"$"}{XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${"$"}{(%):-%n}.zsh"
    fi

    (( ${"$"}{+commands[direnv]} )) && emulate zsh -c "$(direnv hook zsh)"
  '';
  # Extra commands that should be added to .zshrc before compinit.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.localVariables = { };
  # Extra local variables defined at the top of .zshrc.
  # Type: attribute set
  # Default: { }
  # Example: { POWERLEVEL9K_LEFT_PROMPT_ELEMENTS = [ "dir" "vcs" ] ; }
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.loginExtra = "";
  # Extra commands that should be added to .zlogin.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.logoutExtra = "";
  # Extra commands that should be added to .zlogout.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.oh-my-zsh.enable = true;
  # Whether to enable oh-my-zsh.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.oh-my-zsh.custom = "";
  # Path to a custom oh-my-zsh package to override config of oh-my-zsh. See https://github.com/robbyrussell/oh-my-zsh/wiki/Customization for more information.
  # Type: string
  # Default: ""
  # Example: "\$HOME/my_customizations"
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.oh-my-zsh.extraConfig = "";
  # Extra settings for plugins.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Example:
  # ''
  # zstyle :omz:plugins:ssh-agent identities id_rsa id_rsa2 id_github
  # ''
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.oh-my-zsh.plugins = [ "git" "fzf" ];
  # List of oh-my-zsh plugins
  # Type: list of strings
  # Default: [ ]
  # Example: [ "git" "sudo" ]
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.oh-my-zsh.theme = "";
  # Name of the theme to be used by oh-my-zsh.
  # Type: string
  # Default: ""
  # Example: "robbyrussell"
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.plugins = [ ];
  # Plugins to source in .zshrc.
  # Type: list of submodules
  # Default: [ ]
  # Example:
  # [
  #   {
  #     # will source zsh-autosuggestions.plugin.zsh
  #     name = "zsh-autosuggestions";
  #     src = pkgs.fetchFromGitHub {
  #       owner = "zsh-users";
  #       repo = "zsh-autosuggestions";
  #       rev = "v0.4.0";
  #       sha256 = "0z6i9wjjklb4lvr7zjhbphibsyx51psv50gm07mbb0kj9058j6kc";
  #     };
  #   }
  #   {
  #     name = "enhancd";
  #     file = "init.sh";
  #     src = pkgs.fetchFromGitHub {
  #       owner = "b4b4r07";
  #       repo = "enhancd";
  #       rev = "v2.2.1";
  #       sha256 = "0iqa9j09fwm6nj5rpip87x3hnvbbz9w9ajgm6wkrd5fls8fn8i5g";
  #     };
  #   }
  # ]
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  # programs.zsh.plugins.*.file
  # The plugin script to source.
  # Type: string
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  # programs.zsh.plugins.*.name
  # The name of the plugin. Don't forget to add file if the script name does not follow convention.
  # Type: string
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  # programs.zsh.plugins.*.src
  # Path to the plugin folder. Will be added to fpath and PATH.
  # Type: path
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.profileExtra = "";
  # Extra commands that should be added to .zprofile.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.sessionVariables = {
    # Environment variables that will be set for zsh session.
    # Type: attribute set
    # Default: { }
    # Example: { MAILCHECK = 30; }
    # Declared by:
    # <home-manager/modules/programs/zsh.nix> 
    FZF_BASE = "/run/current-system/sw/bin/fzf";
    FZF_DEFAULT_COMMAND = "rg --files --hidden";
    TERM = "st-256color";
    KEYTIMEOUT = 1;
    YSU_MESSAGE_FORMAT = "$(tput setaf 4)You should: %command ➜  %alias$(tput sgr0)";
  };

  programs.zsh.shellAliases = {
    # An attribute set that maps aliases (the top level attribute names in this option) to command strings or directly to build outputs.
    # Type: attribute set of strings
    # Default: { }
    # Example:
    # {
    #   ll = "ls -l";
    #   ".." = "cd ..";
    # }
    # Declared by:
    # <home-manager/modules/programs/zsh.nix> 
    ls = "exa --icons --group-directories-first";
    wget = "wget --hsts-file=\"$XDG_CACHE_HOME/wget-hsts\"";
    grep = "grep --color=auto";
    c = "cd";
    v = "nvim";
    r = "ranger";
    vifm = "vifmrun";

    yt = "youtube-dl --add-metadata -i";
    yta = "yt -x -f bestaudio/best";
  };

  programs.zsh.shellGlobalAliases = { };
  # Similar to opt-programs.zsh.shellAliases, but are substituted anywhere on a line.
  # Type: attribute set of strings
  # Default: { }
  # Example:
  # {
  #   UUID = "$(uuidgen | tr -d \\n)";
  #   G = "| grep";
  # }
  # Declared by:
  # <home-manager/modules/programs/zsh.nix> 

  programs.zsh.zplug.enable = false;
  # Whether to enable zplug - a zsh plugin manager.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/programs/zplug.nix> 

  programs.zsh.zplug.plugins = [ ];
  # List of zplug plugins.
  # Type: list of submodules
  # Default: [ ]
  # Declared by:
  # <home-manager/modules/programs/zplug.nix> 

  # programs.zsh.zplug.plugins.*.name
  # The name of the plugin.
  # Type: string
  # Declared by:
  # <home-manager/modules/programs/zplug.nix> 

  # programs.zsh.zplug.plugins.*.tags
  # The plugin tags.
  # Type: list of strings
  # Default: [ ]
  # Declared by:
  # <home-manager/modules/programs/zplug.nix> 
}
