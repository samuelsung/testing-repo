{ ... }:
{
  xdg.enable = true;
  # Whether to enable management of XDG base directories.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.cacheHome = ~/.cache;
  # Absolute path to directory holding application caches.
  # Type: path
  # Default: "~/.cache"
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.configFile
  # Attribute set of files to link into the user's XDG configuration home.
  # Type: attribute set of submodules
  # Default: { }
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.configFile.<name>.executable
  # Set the execute bit. If null, defaults to the mode of the source file or to false for files created through the text option.
  # Type: null or boolean
  # Default: null
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.configFile.<name>.onChange
  # Shell commands to run when file has changed between generations. The script will be run after the new files have been linked into place.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.configFile.<name>.recursive
  # If the file source is a directory, then this option determines whether the directory should be recursively linked to the target location. This option has no effect if the source is a file.
  # If false (the default) then the target will be a symbolic link to the source directory. If true then the target will be a directory structure matching the source's but whose leafs are symbolic links to the files of the source directory.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.configFile.<name>.source
  # Path of the source file or directory. If home.file.<name?>.text is non-null then this option will automatically point to a file containing that text.
  # Type: path
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.configFile.<name>.target
  # Path to target file relative to xdg.configHome.
  # Type: string
  # Default: <name>
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.configFile.<name>.text
  # Text of the file. If this option is null then home.file.<name?>.source must be set.
  # Type: null or strings concatenated with "\n"
  # Default: null
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.configHome = ~/.config;
  # Absolute path to directory holding application configurations.
  # Type: path
  # Default: "~/.config"
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.dataFile
  # Attribute set of files to link into the user's XDG data home.
  # Type: attribute set of submodules
  # Default: { }
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.dataFile.<name>.executable
  # Set the execute bit. If null, defaults to the mode of the source file or to false for files created through the text option.
  # Type: null or boolean
  # Default: null
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.dataFile.<name>.onChange
  # Shell commands to run when file has changed between generations. The script will be run after the new files have been linked into place.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.dataFile.<name>.recursive
  # If the file source is a directory, then this option determines whether the directory should be recursively linked to the target location. This option has no effect if the source is a file.
  # If false (the default) then the target will be a symbolic link to the source directory. If true then the target will be a directory structure matching the source's but whose leafs are symbolic links to the files of the source directory.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.dataFile.<name>.source
  # Path of the source file or directory. If home.file.<name?>.text is non-null then this option will automatically point to a file containing that text.
  # Type: path
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.dataFile.<name>.target
  # Path to target file relative to xdg.dataHome.
  # Type: string
  # Default: <name>
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.dataFile.<name>.text
  # Text of the file. If this option is null then home.file.<name?>.source must be set.
  # Type: null or strings concatenated with "\n"
  # Default: null
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  # xdg.dataHome = ~/.local/share;
  # Absolute path to directory holding application data.
  # Type: path
  # Default: "~/.local/share"
  # Declared by:
  # <home-manager/modules/misc/xdg.nix> 

  xdg.mime.enable = true;
  # Whether to install programs and files to support the XDG Shared MIME-info specification and XDG MIME Applications specification at https://specifications.freedesktop.org/shared-mime-info-spec/shared-mime-info-spec-latest.html and https://specifications.freedesktop.org/mime-apps-spec/mime-apps-spec-latest.html, respectively.
  # Type: boolean
  # Default: true
  # Declared by:
  # <home-manager/modules/misc/xdg-mime.nix> 

  xdg.mimeApps.enable = true;
  # Whether to manage $XDG_CONFIG_HOME/mimeapps.list.
  # The generated file is read-only.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/misc/xdg-mime-apps.nix> 

  xdg.mimeApps.associations.added = {
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document" = "writer.desktop";
    "application/x-extension-htm" = [ "firefox.desktop" ];
    "application/x-extension-html" = [ "firefox.desktop" ];
    "application/x-extension-shtml" = [ "firefox.desktop" ];
    "application/x-extension-xhtml" = [ "firefox.desktop" ];
    "application/x-extension-xht" = [ "firefox.desktop" ];
    "application/xhtml+xml" = [ "firefox.desktop" ];
    "text/html" = [ "firefox.desktop" ];
    "x-scheme-handler/chrome" = [ "firefox.desktop" ];
    "x-scheme-handler/http" = [ "firefox.desktop" ];
    "x-scheme-handler/https" = [ "firefox.desktop" ];
  };
  # Defines additional associations of applications with mimetypes, as if the .desktop file was listing this mimetype in the first place.
  # Type: attribute set of list of strings or list of strings or string convertible to its
  # Default: { }
  # Example:
  # {
  #   "mimetype1" = [ "foo1.desktop" "foo2.desktop" "foo3.desktop" ];
  #   "mimetype2" = "foo4.desktop";
  # }
  # Declared by:
  # <home-manager/modules/misc/xdg-mime-apps.nix> 

  xdg.mimeApps.associations.removed = { };
  # Removes associations of applications with mimetypes, as if the .desktop file was not listing this mimetype in the first place.
  # Type: attribute set of list of strings or list of strings or string convertible to its
  # Default: { }
  # Example: { mimetype1 = "foo5.desktop"; }
  # Declared by:
  # <home-manager/modules/misc/xdg-mime-apps.nix> 

  # The trash teams will override the mimeapps.list whenever I upload a picture even if there is no changes to the list
  # SEE: https://github.com/nix-community/home-manager/issues/1213
  xdg.configFile."mimeapps.list".force = true;

  xdg.mimeApps.defaultApplications = {
    # The default application to be used for a given mimetype. This is, for instance, the one that will be started when double-clicking on a file in a file manager. If the application is no longer installed, the next application in the list is attempted, and so on.
    # Type: attribute set of list of strings or list of strings or string convertible to its
    # Default: { }
    # Example:
    # {
    #   "mimetype1" = [ "default1.desktop" "default2.desktop" ];
    # }
    # Declared by:
    # <home-manager/modules/misc/xdg-mime-apps.nix> 
    "application/x-extension-htm" = [ "firefox.desktop" ];
    "application/x-extension-html" = [ "firefox.desktop" ];
    "application/x-extension-shtml" = [ "firefox.desktop" ];
    "application/x-extension-xht" = [ "firefox.desktop" ];
    "application/x-extension-xhtml" = [ "firefox.desktop" ];
    "application/xhtml+xml" = [ "firefox.desktop" ];
    "inode/directory" = [ "vifm.desktop" ];
    "inode/mount-point" = [ "vifm.desktop" ];
    "text/html" = [ "firefox.desktop" ];
    "x-scheme-handler/chrome" = [ "firefox.desktop" ];
    "x-scheme-handler/figma" = [ "figma-linux.desktop" ];
    "x-scheme-handler/http" = [ "firefox.desktop" ];
    "x-scheme-handler/https" = [ "firefox.desktop" ];
    "x-scheme-handler/msteams" = [ "teams.desktop" ];
  };

  xdg.userDirs.enable = true;
  # Whether to manage $XDG_CONFIG_HOME/user-dirs.dirs.
  # The generated file is read-only.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 

  xdg.userDirs.desktop = "";
  # The Desktop directory.
  # Type: string
  # Default: "\$HOME/Desktop"
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 

  xdg.userDirs.documents = "";
  # The Documents directory.
  # Type: string
  # Default: "\$HOME/Documents"
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 

  xdg.userDirs.download = "\$HOME/downloads";
  # The Downloads directory.
  # Type: string
  # Default: "\$HOME/Downloads"
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 

  xdg.userDirs.extraConfig = { };
  # Other user directories.
  # Type: attribute set of strings
  # Default: { }
  # Example: { XDG_MISC_DIR = "\$HOME/Misc"; }
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 

  xdg.userDirs.music = "";
  # The Music directory.
  # Type: string
  # Default: "\$HOME/Music"
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 

  xdg.userDirs.pictures = "";
  # The Pictures directory.
  # Type: string
  # Default: "\$HOME/Pictures"
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 

  xdg.userDirs.publicShare = "";
  # The Public share directory.
  # Type: string
  # Default: "\$HOME/Public"
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 

  xdg.userDirs.templates = "";
  # The Templates directory.
  # Type: string
  # Default: "\$HOME/Templates"
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 

  xdg.userDirs.videos = "";
  # The Videos directory.
  # Type: string
  # Default: "\$HOME/Videos"
  # Declared by:
  # <home-manager/modules/misc/xdg-user-dirs.nix> 
}
