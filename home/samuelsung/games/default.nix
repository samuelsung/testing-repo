{ pkgs, ... }:

{
  home.packages= with pkgs; [
    # Mini games
    kmines
    (custom.osu.override {
      appimageTools = pkgs.appimageTools.override {
        buildFHSUserEnv = pkgs.buildFHSUserEnv;
      };
    })
    lutris
    steam
    unstable.protontricks
  ];
}
