{ withProxyBlock ? true
, withGames ? false
, withSocialMedia ? false
, withShopping ? false
, withSecretProfile ? false
}:

{ pkgs, lib, ... }:

with lib;
with (import ./lib.nix { inherit pkgs lib; });
let
  settings = import ./settings.nix { inherit lib withProxyBlock; };
  oneTimeSettings = {
    # UI

    ## Hide toolbars
    "browser.toolbars.bookmarks.visibility" = "never";

    ## Inspect
    "devtools.toolbox.host" = "window";

    # Downloads

    ## Downloads will go to the Downloads folder
    "browser.download.folderList" = 1;

    ## Ask download location everytime
    "browser.download.useDownloadDir" = false;

    # Findbar

    ## match part of a word
    "findbar.entireword" = false;

    ## highlight all match result
    "findbar.highlightAll" = true;

  };

  # These two is copied from the home-manager repos
  # TODO: Use home-manger options after 21.11 launch
  # TODO: Since folder structure has been added, see if it will still fit after 21.11 launch
  # https://github.com/nix-community/home-manager/commit/e0a87d75e9083569f73efc47b58c0e3fa4f99382
  firefoxBookmarksFile =
    { toolbar ? [ ]
    , menu ? [ ]
    }:
    let
      escapeXML = replaceStrings [ ''"'' "'" "<" ">" "&" ] [
        "&quot;"
        "&apos;"
        "&lt;"
        "&gt;"
        "&amp;"
      ];
      entryMapper = entry:
        ''
          <DT><A HREF="${escapeXML entry.url}" ADD_DATE="0" LAST_MODIFIED="0"${
            lib.optionalString (entry.keyword != null)
            " SHORTCUTURL=\"${escapeXML entry.keyword}\""
          }>${escapeXML entry.name}</A>
        '';

      folderMapper = { isFolder ? false, name ? "", ... }@entry:
        if isFolder then
          ''
            <DT><H3>${escapeXML name}</H3>
            <DL><p>
              ${concatStrings (builtins.map folderMapper entry.children)}
            </DL><p>
          ''
        else entryMapper entry;

      bookmarksEntries = builtins.map folderMapper;

    in
    pkgs.writeText "firefox-bookmarks.html" ''
      <!DOCTYPE NETSCAPE-Bookmark-file-1>
      <!-- This is an automatically generated file.
        It will be read and overwritten.
        DO NOT EDIT! -->
      <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
      <TITLE>Bookmarks</TITLE>
      <H1>Bookmarks Menu</H1>
      <DL><p>
        ${concatStrings (bookmarksEntries menu)}
        <DT><H3 ADD_DATE="0" LAST_MODIFIED="0" PERSONAL_TOOLBAR_FOLDER="true">Bookmarks Toolbar</H3>
        <DL><p>
          ${concatStrings (bookmarksEntries toolbar)}
        </DL><p>
      </DL>
    '';

  bookmarkSettings = bookmarks: {
    "browser.bookmarks.file" = toString (firefoxBookmarksFile bookmarks);
    "browser.places.importBookmarksHTML" = true;
  };
in
{
  programs.firefox.enable = true;
  # Whether to enable Firefox.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.enableGnomeExtensions = false;
  # Whether to enable the GNOME Shell native host connector. Note, you also need to set the NixOS option services.gnome3.chrome-gnome-shell.enable to true.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  programs.firefox.package =
    let
      mapLockPref = prefs: concatStrings (mapAttrsToList
        (name: value: ''
          lockPref("${name}", ${builtins.toJSON value});
        '')
        prefs);
    in
    pkgs.firefox.override {
      extraPrefs = ''
        ${mapLockPref ((foldl (a: b: a // b) { } (attrValues settings)) // bookmarkSettings (import ./bookmarks.nix))}
      '';
    };
  # The Firefox package to use. If state version ≥ 19.09 then this should be a wrapped Firefox package. For earlier state versions it should be an unwrapped Firefox package.
  # Type: package
  # Default: pkgs.firefox
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  programs.firefox.extensions = with pkgs.nur.repos.rycee.firefox-addons; [
    https-everywhere
    privacy-badger
    vimium
    h264ify
    bitwarden
    reduxdevtools
    react-devtools
    ublock-origin
  ];
  # List of Firefox add-on packages to install. Some pre-packaged add-ons are accessible from NUR, https://github.com/nix-community/NUR. Once you have NUR installed run
  #   $ nix-env -f '<nixpkgs>' -qaP -A nur.repos.rycee.firefox-addons
  # to list the available Firefox add-ons.
  # Note that it is necessary to manually enable these extensions inside Firefox after the first installation.
  # Extensions listed here will only be available in Firefox profiles managed through the programs.firefox.profiles option. This is due to recent changes in the way Firefox handles extension side-loading.
  # Type: list of packages
  # Default: [ ]
  # Example:
  # with pkgs.nur.repos.rycee.firefox-addons; [
  #   https-everywhere
  #   privacy-badger
  # ]
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  programs.firefox.profiles = {
    default = {
      id = 0;
      isDefault = true;
      settings = oneTimeSettings;
    };

  } // (if withSecretProfile then {

    secret = {
      id = 1;
      isDefault = false;

      settings = oneTimeSettings // {
        # NOTE: bookmark.html will be updated on nix-rebuild since sops-nix does not support home manager yet.
        # see: https://github.com/Mic92/sops-nix/issues/62
        "browser.bookmarks.file" = "/run/secrets/bookmark.html";
        "browser.places.importBookmarksHTML" = true;
      };
    };

  } else { });

  # Attribute set of Firefox profiles.
  # Type: attribute set of submodules
  # Default: { }
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.bookmarks
  # Preloaded bookmarks. Note, this may silently overwrite any previously existing bookmarks!
  # Type: attribute set of submodules
  # Default: { }
  # Example:
  # {
  #   wikipedia = {
  #     keyword = "wiki";
  #     url = "https://en.wikipedia.org/wiki/Special:Search?search=%s&go=Go";
  #   };
  #   "kernel.org" = {
  #     url = "https://www.kernel.org";
  #   };
  # }
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.bookmarks.<name>.keyword
  # Bookmark search keyword.
  # Type: null or string
  # Default: null
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.bookmarks.<name>.name
  # Bookmark name.
  # Type: string
  # Default: "‹name›"
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 
  # programs.firefox.profiles.<name>.bookmarks.<name>.url
  # Bookmark url, use %s for search terms.
  # Type: string
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.extraConfig
  # Extra preferences to add to user.js.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.id
  # Profile ID. This should be set to a unique number per profile.
  # Type: unsigned integer, meaning >=0
  # Default: 0
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.isDefault
  # Whether this is a default profile.
  # Type: boolean
  # Default: "true if profile ID is 0"
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.name
  # Profile name.
  # Type: string
  # Default: "‹name›"
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.path
  # Profile path.
  # Type: string
  # Default: "‹name›"
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.settings
  # Attribute set of Firefox preferences.
  # Type: attribute set of boolean or signed integer or strings
  # Default: { }
  # Example:
  # {
  #   "browser.startup.homepage" = "https://nixos.org";
  #   "browser.search.region" = "GB";
  #   "browser.search.isUS" = false;
  #   "distribution.searchplugins.defaultLocale" = "en-GB";
  #   "general.useragent.locale" = "en-GB";
  #   "browser.bookmarks.showMobileBookmarks" = true;
  # }
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.userChrome
  # Custom Firefox user chrome CSS.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Example:
  # ''
  # /* Hide tab bar in FF Quantum */
  # @-moz-document url("chrome://browser/content/browser.xul") {
  #   #TabsToolbar {
  #     visibility: collapse !important;
  #     margin-bottom: 21px !important;
  #   }

  #   #sidebar-box[sidebarcommand="treestyletab_piro_sakura_ne_jp-sidebar-action"] #sidebar-header {
  #     visibility: collapse !important;
  #   }
  # }
  # ''
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.userContent
  # Custom Firefox user content CSS.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Example:
  # ''
  # /* Hide scrollbar in FF Quantum */
  # *{scrollbar-width:none !important}
  # ''
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # Set default search to DuckDuckGo.
  # Also set search shortcuts
  home.file = {
    ".mozilla/firefox/default/search.json.mozlz4" = {
      source = mapMozlz4 "default" (import ./searchEngines.nix {
        inherit
          withGames
          withSocialMedia
          withShopping;
      });
      force = true;
    };

  } // (if withSecretProfile then {

    ".mozilla/firefox/secret/search.json.mozlz4" = {
      source = mapMozlz4 "secret" (import ./searchEngines.nix {
        inherit
          withGames
          withSocialMedia
          withShopping;
      });
      force = true;
    };

  } else { });
}
