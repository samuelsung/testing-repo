{ pkgs, lib }:

let
  inherit (pkgs) runCommand;
  inherit (builtins) readFile toJSON;
in
rec {
  getBase64 = type: path:
    let
      base64 = runCommand (toString path)
        {
          inherit path;
          preferLocalBuild = true;
          allowSubstitutes = false;
          # nativeBuildInputs = [ pkgs.base64 ];
        }
        ''
          base64 "$path" > "$out";
        '';
    in
    ''data:image/${type};base64,${readFile base64}'';

  mapEngine = order:
    { name
    , isAppProvided ? false
    , loadPath ? ""
    , loadPathHash ? ""
    , searchForm ? null
    , desc ? ""
    , alias ? ""
    , icon ? null
    , iconType ? "png"
    , urls ? ""
    , hidden ? false
    }: {
      "_name" = name;
      "_isAppProvided" = isAppProvided;
      "_metaData" = {
        "loadPathHash" = loadPathHash;
        "order" = order;
        "alias" = alias;
        "hidden" = hidden;
      };
    } // (if isAppProvided then { } else {
      "_loadPath" = loadPath;
      "description" = desc;
      "__searchForm" = searchForm;
      "_iconURL" = getBase64 iconType icon;
      "_urls" = urls;
      "_orderHint" = null;
      "_telemetryId" = null;
      "_updateInterval" = null;
      "_updateURL" = null;
      "_iconUpdateURL" = null;
      "_filePath" = null;
      "_extensionID" = null;
      "_locale" = null;
      "_definedAliases" = [ ];
    });

  mapSearchFile =
    { version
    , engines
    , metaData
    }: toJSON {
      inherit version metaData;
      engines = lib.imap0 mapEngine engines;
    };

  writeMozlz4 = name: config: runCommand name
    {
      inherit config;
      preferLocalBuild = true;
      allowSubstitutes = false;
      nativeBuildInputs = with pkgs; [ mozlz4a jq ];
    }
    ''
      echo "$config" | jq --compact-output > compact
      mozlz4a compact "$out"
    '';

  mapMozlz4 = name: config: writeMozlz4 name (mapSearchFile config);
}
