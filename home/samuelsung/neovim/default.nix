{ programs, pkgs, ... }:

let
  php-vim = pkgs.vimUtils.buildVimPlugin {
    name = "php.vim";
    src = pkgs.fetchFromGitHub {
      owner = "StanAngeloff";
      repo = "php.vim";
      rev = "930aec5c7026297a6630bd2940c08c5ff552cf2a";
      sha256 = "sxrenhhNvpfJNCOKlaLvSvLjUSKtseZpWbeULmOu2yY=";
    };
  };

  sxhkd-vim = pkgs.vimUtils.buildVimPlugin {
    name = "sxhkd-vim";
    src = pkgs.fetchFromGitHub {
      owner = "kovetskiy";
      repo = "sxhkd-vim";
      rev = "2760f9d32bd2fb4d7d5305a88eb2056d149c6484";
      sha256 = "NPf8chzqLuA7V5aNQcehLzJCNmFn/Vol4DuGtIfaDDE=";
    };
  };

  francoiscabrol-ranger-vim = pkgs.vimUtils.buildVimPlugin {
    name = "ranger.vim";
    src = pkgs.fetchFromGitHub {
      owner = "francoiscabrol";
      repo = "ranger.vim";
      rev = "91e82debdf566dfaf47df3aef0a5fd823cedf41c";
      sha256 = "6ut7u6AwtyYbHLHa2jelf5PkbtlfHvuHfWRL5z1CTUQ=";
    };
  };

  vifm-vim = pkgs.vimUtils.buildVimPlugin {
    name = "vifm.vim";
    src = pkgs.fetchFromGitHub {
      owner = "vifm";
      repo = "vifm.vim";
      rev = "d174fbba22c0b3761127931c08e91c3e35171f5f";
      sha256 = "00nsqgykcda307xzg8992kxxrxfdyab8pgyj3lpzd8k05wm6w938";
    };
  };

  blamer-nvim = pkgs.vimUtils.buildVimPlugin {
    name = "blamer.nvim";
    src = pkgs.fetchFromGitHub {
      owner = "APZelos";
      repo = "blamer.nvim";
      rev = "11442047a3e00bce09b076e910e4e4a29bbebba0";
      sha256 = "15ma1396zvmif3ysjq5rnl5nkycfixaiv3gl8lz1lkzjm7spk6b1";
    };
  };
  # vibusen-vim = pkgs.vimUtils.buildVimPlugin {
  #   name = "vibusen.vim";
  #   src = pkgs.fetchFromGitHub {
  #     owner = "lsrdg";
  #     repo = "vibusen.vim";
  #     rev = "9d944ea023253d35351e672eb2742ddcf1445355";
  #     sha256 = "1n2s8b7kya8dnn1d5b0dc8yadl92iwf58s7sb5950b6yyi3i3q7f";
  #   };
  # };
in
{
  nixpkgs.overlays = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/neovim-nightly-overlay/archive/master.tar.gz;
    }))
  ];

  ### Basics
  programs.neovim.enable = true;
    # Whether to enable Neovim.
    # Type: boolean
    # Default: false
    # Example: true
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>

  programs.neovim.package = pkgs.neovim-nightly;
    # The package to use for the neovim binary.
    # Type: package
    # Default: pkgs.neovim-unwrapped
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>

  ### Alias
  programs.neovim.viAlias = true;
    # Symlink vi to nvim binary.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>
  programs.neovim.vimAlias = true;
    # Symlink vim to nvim binary.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>
  programs.neovim.vimdiffAlias = true;
    # Alias vimdiff to nvim -d.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>

  ### Providers
  programs.neovim.withNodeJs = true;
    # Enable node provider. Set to true to use Node plugins.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>
  programs.neovim.withPython = false;
    # Enable Python 2 provider. Set to true to use Python 2 plugins.
    # Type: boolean
    # Default: true
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>
  programs.neovim.withPython3 = true;
    # Enable Python 3 provider. Set to true to use Python 3 plugins.
    # Type: boolean
    # Default: true
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>
  programs.neovim.withRuby = true;
    # Enable ruby provider.
    # Type: null or boolean
    # Default: true
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>

  # Plugins
  programs.neovim.plugins = with pkgs.vimPlugins; [
    # List of vim plugins to install optionally associated with configuration to be placed in init.vim.
    # This option is mutually exclusive with configure.
    # Type: list of package or submodules
    # Default: [ ]
    # Example:
    # with pkgs.vimPlugins; [
    #   yankring
    #   vim-nix
    #   { plugin = vim-startify;
    #     config = "let g:startify_change_to_vcs_root = 0";
    #   }
    # ]
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>

    ### Themes
    # gruvbox
    nord-vim
    vim-airline
    vim-airline-themes
    # vim-devicons
    vim-bufferline
    # lightline-vim
    indentLine
    # vim-nerdtree-syntax-highlight

    # Coding
    coc-nvim
    coc-snippets
    coc-tsserver
    coc-eslint
    coc-prettier
    coc-pairs
    coc-json
    coc-css
    coc-java
    coc-python
    coc-rls

    # Ranger
    francoiscabrol-ranger-vim
    vifm-vim
    bclose-vim

    # Git
    vim-gitgutter
    blamer-nvim

    # Navigation
    fzf-vim
    nerdtree
    # vibusen-vim
    # ctrlp-vim
    nerdcommenter
    vim-table-mode
    vim-easymotion

    ### language
    # Javascript
    vim-javascript-syntax
    # JSX
    vim-jsx-pretty
    # Typescript
    yats-vim
    # Rust
    rust-vim
    # Toml
    vim-toml
    # JSON
    vim-json
    # Markdown
    tabular
    vim-markdown
    # Kotlin
    kotlin-vim
    # PHP
    php-vim
    # sxhkd
    sxhkd-vim
    # Yaml
    vim-yaml
    # Nix
    vim-nix
  ];

  # programs.neovim.extraPackages = [];
    # Extra packages available to nvim.
    # Type: list of packages
    # Default: [ ]
    # Example: "[ pkgs.shfmt ]"
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>

  # programs.neovim.extraPython3Packages = []; # deprecated
    # A function in python.withPackages format, which returns a list of Python 3 packages required for your plugins to work.
    # Type: python3 packages in python.withPackages format or list of packages
    # Default: "ps: []"
    # Example:
    # (ps: with ps; [ python-language-server ])
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>

  # programs.neovim.extraPythonPackages = []; # deprecated
    # A function in python.withPackages format, which returns a list of Python 2 packages required for your plugins to work.
    # Type: python packages in python.withPackages format or list of packages
    # Default: "ps: []"
    # Example:
    # (ps: with ps; [ pandas jedi ])
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>

  # Configurations
  # programs.neovim.configure = {};
    # Generate your init file from your list of plugins and custom commands, and loads it from the store via nvim -u /nix/store/hash-vimrc
    # This option is mutually exclusive with extraConfig and plugins.
    # Type: attribute set
    # Default: { }
    # Example:
    # configure = {
    #     customRC = $'''
    #     " here your custom configuration goes!
    #     $''';
    #     packages.myVimPackage = with pkgs.vimPlugins; {
    #       # loaded on launch
    #       start = [ fugitive ];
    #       # manually loadable by calling `:packadd $plugin-name`
    #       opt = [ ];
    #     };
    #   };
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>

  home.file.".config/nvim/coc-settings.json".source = ./coc-settings.json;
    # Coc configuration file

  programs.neovim.extraConfig =
    # Custom vimrc lines.
    # This option is mutually exclusive with configure.
    # Type: strings concatenated with "\n"
    # Default: ""
    # Example:
    # ''
    # set nocompatible
    # set nobackup
    # ''
    # Declared by:
    # <home-manager/modules/programs/neovim.nix>
  ''
    ${builtins.readFile ./general.vim}
    ${builtins.readFile ./layout.vim}
    ${builtins.readFile ./indent.vim}
    ${builtins.readFile ./language.vim}
    ${builtins.readFile ./navigation_colemak.vim}
    ${builtins.readFile ./plug-config/ranger.vim}
    ${builtins.readFile ./plug-config/vifm.vim}
    ${builtins.readFile ./plug-config/coc.vim}
    ${builtins.readFile ./plug-config/NERDCommenter.vim}
    ${builtins.readFile ./plug-config/fzf.vim}
    ${builtins.readFile ./plug-config/gitgutter.vim}
    ${builtins.readFile ./plug-config/blamer.vim}
  '';
    # let g:IbusDefaultEngine = 'xkb:us:colemak:eng'
}

