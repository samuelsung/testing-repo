{ ... }:

{
  dconf.settings = {
    "desktop/ibus/general" = {
      preload-engines = ["xkb:us:colemak:eng" "mozc-jp" "xkb:us::eng"];
      engines-order = ["xkb:us:colemak:eng" "mozc-jp" "xkb:us::eng"];
    };
    "desktop/ibus/general/hotkey" = {
      triggers = ["<Alt><Super>space"];
    };
    "org/freedesktop/ibus/engine/anthy/common" = {
      input-mode = 0;
    };
  };
}
