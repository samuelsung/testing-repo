{ programs, pkgs, ... }:

{
  home.packages = with pkgs; [
    polybarFull
  ];

  home.file.".config/polybar/colors".source = ./configs/colors;
  home.file.".config/polybar/battery".source = ./configs/battery;
  home.file.".config/polybar/config".source = ./configs/config;
  home.file.".config/polybar/cpu".source = ./configs/cpu;
  home.file.".config/polybar/date".source = ./configs/date;
  home.file.".config/polybar/filesystem".source = ./configs/filesystem;
  home.file.".config/polybar/launch.sh".source = ./configs/launch.sh;
  home.file.".config/polybar/layout".source = ./configs/layout;
  home.file.".config/polybar/memory".source = ./configs/memory;
  home.file.".config/polybar/pulseaudio".source = ./configs/pulseaudio;
  home.file.".config/polybar/settings".source = ./configs/settings;
  home.file.".config/polybar/title".source = ./configs/title;
  home.file.".config/polybar/workspace".source = ./configs/workspace;
}
