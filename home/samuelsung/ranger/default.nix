{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    ueberzug
    ranger
  ];

  home.file.".config/ranger/commands.py".source = ./commands.py;
  home.file.".config/ranger/rc.conf".source = ./rc.conf;
  home.file.".config/ranger/rifle.conf".source = ./rifle.conf;
  home.file.".config/ranger/scope.sh".source = ./scope.sh;
  home.file.".config/ranger/shortcuts.conf".source = ./shortcuts.conf;
}
