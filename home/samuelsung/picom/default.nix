{ programs, pkgs, ... }:

{
  services.picom.enable = true;
  # Whether to enable Picom X11 compositor.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/services/picom.nix> 

  services.picom.package = pkgs.picom;
  # picom derivation to use.
  # Type: package
  # Default: pkgs.picom
  # Example:
  # pkgs.picom
  # Declared by:
  # <home-manager/modules/services/picom.nix> 

  home.file.".config/picom.conf".source = ./picom.conf;
}
