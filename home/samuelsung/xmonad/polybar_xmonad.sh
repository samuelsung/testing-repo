#!/bin/sh

nord0=#2E3440
nord1=#3B4252
nord2=#434C5E
nord3=#4C566A
nord4=#D8DEE9
nord5=#E5E9F0
nord6=#ECEFF4
nord7=#8FBCBB
nord8=#88C0D0
nord9=#81A1C1
nord10=#5E81AC
nord11=#BF616A
nord12=#D08770
nord13=#EBCB8B
nord14=#A3BE8C
nord15=#B48EAD

nord0withalpha=#D02E3440
nord9withalpha=#D081A1C1

exit_script() {
  echo "Unknown command";
  exit 1;
}
number_mapper() { # $1 style $2 number
  case $1 in
    fill)
      case $2 in
        1)
          echo 
          ;;
        2)
          echo 
          ;;
        3)
          echo 
          ;;
        4)
          echo 
          ;;
        5)
          echo 
          ;;
        6)
          echo 
          ;;
        7)
          echo 
          ;;
        8)
          echo 
          ;;
        9)
          echo 
          ;;
      esac
      ;;
    outline)
      case $2 in
        1)
          echo 
          ;;
        2)
          echo 
          ;;
        3)
          echo 
          ;;
        4)
          echo 
          ;;
        5)
          echo 
          ;;
        6)
          echo 
          ;;
        7)
          echo 
          ;;
        8)
          echo 
          ;;
        9)
          echo 
          ;;
      esac
      ;;
    outline-pop)
      case $2 in
        1)
          echo 
          ;;
        2)
          echo 
          ;;
        3)
          echo 
          ;;
        4)
          echo 
          ;;
        5)
          echo 
          ;;
        6)
          echo 
          ;;
        7)
          echo 
          ;;
        8)
          echo 
          ;;
        9)
          echo 
          ;;
      esac
      ;;
    *)
      ;;
  esac
}

case $1 in
  watch)
    case $2 in
      workspace)
        [ -z "$3" ] && exit_script;
        dbus-monitor "type='signal',path='/org/xmonad/Log',interface='org.xmonad.Log',member='Update'" | while read foo; do {
          desktop=$(echo $foo | grep -o "$3::ws_.*::$3");
          case $desktop in
            *current::*)
              # echo $foo | grep "::title::" && echo "%{F$nord1}%{B$nord9withalpha} %{o$nord1}%{+o}$(number_mapper outline-pop $3)%{-o} %{B-}%{F-}" || echo "%{F$nord1}%{B$nord9withalpha} $(number_mapper outline-pop $3) %{B-}%{F-}";
              echo $foo | grep -q "::title::" && echo "%{F$nord6} %{o$nord6}%{+o}$(number_mapper outline-pop $3)%{-o} %{F-}" || echo "%{F$nord6} $(number_mapper outline-pop $3) %{F-}";
              ;;
            *visable::*)
              echo "%{F$nord4}%{o$nord4}%{+o}$(number_mapper outline-pop $3)%{-o}%{F-}";
              ;;
            *hidden::*)
              echo "%{F$nord4}%{o$nord4}%{+o}$(number_mapper outline $3)%{-o}%{F-}";
              ;;
            *hiddenNoWindows::*)
              echo "%{F$nord4}$(number_mapper outline $3)%{F-}";
              ;;
            *urgent::*)
              echo "$(number_mapper fill $3)";
              ;;
            *)
              ;;
          esac
        } done
        ;;
      layout)
        xmonad-log | while read foo; do {
          layout=$(echo $foo | grep -o "::layout::.*::layout::");
          case $layout in
            "::layout::Spacing Tall::layout::")
              echo " Tile ";
              ;;
            "::layout::Spacing Mirror Tall::layout::")
              echo " Mirror Tile ";
              ;;
            "::layout::Spacing Full::layout::")
              echo " Full ";
              ;;
          esac
        } done
        ;;
      *) exit_script
        ;;
    esac
    ;;
  click)
    case $2 in
      workspace)
        [ -z "$3" ] && exit_script;
          wmctrl -s $(expr $3 - 1)
        ;;
      layout)
        ;;
      *)
        ;;
    esac
    ;;
  *) exit_script
    ;;
esac
