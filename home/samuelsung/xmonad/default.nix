{ programs, pkgs, ... }:

{
  home.packages = with pkgs; [
    dbus
  ];

  xsession.windowManager.xmonad = {
    enable = true;
    enableContribAndExtras = true;
    extraPackages = haskellPackages: [
      haskellPackages.dbus
      pkgs.dbus
    ];
    config = ./xmonad.hs;
  };

  home.file.".xmonad/polybar_xmonad.sh".source = ./polybar_xmonad.sh;
}
