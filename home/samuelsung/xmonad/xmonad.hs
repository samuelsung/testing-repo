--
-- xmonad example config file.
--
-- A template showing all available configuration hooks,
-- and how to override the defaults in your own xmonad.hs conf file.
--
-- Normally, you'd only override those defaults you care about.
--

import XMonad hiding ( (|||) )
import Data.Monoid
import System.Exit
import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.InsertPosition
import XMonad.Util.EZConfig
import XMonad.Util.CustomKeys
import XMonad.Actions.DwmPromote
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Fullscreen hiding (fullscreenEventHook)
import XMonad.Layout.Spacing
import XMonad.Hooks.DynamicLog
import XMonad.Actions.CopyWindow
import XMonad.Util.EZConfig
import XMonad.Actions.RotSlaves
import XMonad.Actions.CycleWS
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.NoBorders


import qualified XMonad.StackSet as W
import qualified Data.Map        as M

import qualified DBus as D
import qualified DBus.Client as D
import qualified Codec.Binary.UTF8.String as UTF8

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal      = "st"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask       = mod4Mask

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]

-- Border colors for unfocused and focused windows, respectively.
--

nord::[String]
nord = ["#2E3440"
  , "#3B4252"
  , "#434C5E"
  , "#4C566A"
  , "#D8DEE9"
  , "#E5E9F0"
  , "#ECEFF4"
  , "#8FBCBB"
  , "#88C0D0"
  , "#81A1C1"
  , "#5E81AC"
  , "#BF616A"
  , "#D08770"
  , "#EBCB8B"
  , "#A3BE8C"
  , "#B48EAD"
  ]

myNormalBorderColor  = nord !! 0
myFocusedBorderColor = nord !! 9

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
myKeys conf = mkKeymap conf $
    [ ("M-<Return>",        spawn $ XMonad.terminal conf                                                                                     )
    , ("M-M1-<Return>",     spawn "samedir"                                                                                                  )
    , ("M-d",               spawn "dmenu_run"                                                                                                )

    , ("M-a a",             spawn $ (XMonad.terminal conf) ++ " -e pulsemixer"                                                               )
    , ("M-a n",             spawn "joplin-desktop"                                                                                           )
    , ("M-a r",             spawn $ (XMonad.terminal conf) ++ " -e newsboat"                                                                 )
    , ("M-a f",             spawn $ (XMonad.terminal conf) ++ " -e vifm ."                                                                   )
    , ("M-a w",             spawn "firefox"                                                                                                  )
    , ("M-a e",             spawn "dmenu_emoji"                                                                                              )

    , ("M-f",               sendMessage ToggleStruts >> toggleWindowSpacingEnabled >> toggleScreenSpacingEnabled >> sendMessage ToggleLayout )

    , ("M-b",               sendMessage ToggleStruts                                                                                         )
    , ("M-<Space>",         dwmpromote                                                                                                       )
    , ("M-n",               windows W.focusDown                                                                                              )
    , ("M-e",               windows W.focusUp                                                                                                )
    , ("M-M1-n",            rotAllDown                                                                                                       )
    , ("M-M1-e",            rotAllUp                                                                                                         )
    , ("M-h",               sendMessage Shrink                                                                                               )
    , ("M-i",               sendMessage Expand                                                                                               )
    , ("M-t",               withFocused $ windows . W.sink                                                                                   )
    , ("M-<Tab>",           sendMessage NextLayout                                                                                           )
    , ("M-u",               sendMessage (IncMasterN 1)                                                                                       )
    , ("M-l",               sendMessage (IncMasterN (-1))                                                                                    )
    , ("M-m",               moveTo Prev HiddenWS                                                                                             )
    , ("M-,",               moveTo Next HiddenWS                                                                                             )
    , ("M-M1-m",            shiftTo Prev HiddenWS >> moveTo Prev HiddenWS                                                                    )
    , ("M-M1-,",            shiftTo Next HiddenWS >> moveTo Next HiddenWS                                                                    )
    , ("M-k",               prevScreen                                                                                                       )
    , ("M-.",               nextScreen                                                                                                       )
    , ("M-M1-k",            shiftPrevScreen >> prevScreen                                                                                    )
    , ("M-M1-.",            shiftNextScreen >> nextScreen                                                                                    )

    , ("M-q",               kill                                                                                                             )
    , ("M-M1-C-S-q",        io (exitWith ExitSuccess))
    , ("M-C-S-q",           spawn "xmonad --recompile; xmonad --restart"                                                                     )]
    ++
    [ ("M-" ++  (show k) , windows (W.view i))
        | (i, k) <- zip (XMonad.workspaces conf) [1 .. 9]]
    ++
    [ ("M-M1-" ++ (show k) , windows (W.shift i) >> windows (W.view i))
        | (i, k) <- zip (XMonad.workspaces conf) [1 .. 9]]
    ++
    [ ("<XF86MonBrightnessUp>",   spawn "light -A 10"                                                                                        )
    , ("<XF86MonBrightnessDown>", spawn "light -U 10"                                                                                        )
    , ("<XF86AudioRaiseVolume>",  spawn "pulsemixer --change-volume +10"                                                                     )
    , ("<XF86AudioLowerVolume>",  spawn "pulsemixer --change-volume -10"                                                                     )
    , ("<XF86AudioPlay>",         spawn "playerctl play-pause"                                                                               )
    , ("<XF86AudioMute>",         spawn "pulsemixer --toggle-mute"                                                                           )]
    -- Resize viewed windows to the correct size
    -- , ((modm,               xK_n     ), refresh)


    -- Move focus to the master window
    --, ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    -- , ((modm,               xK_space), windows W.swapMaster)

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --

    -- Quit xmonad

    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    --, ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
    --]
    -- ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    -- [((m .|. modm, k), windows $ f i)
        -- | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        -- , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
    -- ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    -- [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        -- | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        -- , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayout = avoidStruts $ spacingRaw False (Border 3 3 3 3) True (Border 3 3 3 3) True $ toggleLayouts (noBorders Full) (tiled ||| Mirror tiled)
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.

-- Override the PP values as you would otherwise, adding colors etc depending
-- on  the statusbar used-
--
--
invertWrap::String -> WorkspaceId -> String
invertWrap inner outer = outer ++ inner ++ outer

myLogHook::D.Client -> PP
myLogHook dbus = def { ppOutput = dbusOutput dbus
  , ppCurrent = invertWrap "::ws_current::"
  , ppVisible = invertWrap "::ws_visible::"
  , ppHidden = invertWrap "::ws_hidden::"
  , ppHiddenNoWindows = invertWrap "::ws_hiddenNoWindows::"
  , ppUrgent = invertWrap "::ws_urgent::"
  , ppTitle = wrap "::title::" "::title::"
  , ppLayout = wrap "::layout::" "::layout::"
  , ppSep = " "
  , ppWsSep = " "
}

-- Emit a DBus signal on log updates
dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
    let signal = (D.signal objectPath interfaceName memberName) {
            D.signalBody = [D.toVariant $ UTF8.decodeString str]
        }
    D.emit dbus signal
  where
    objectPath = D.objectPath_ "/org/xmonad/Log"
    interfaceName = D.interfaceName_ "org.xmonad.Log"
    memberName = D.memberName_ "Update"

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
  spawnOnce "feh --bg-fill $HOME/.config/wallpaper --no-fehbg &"
  spawnOnce "pkill -q picom"
  spawnOnce "picom &"
  spawnOnce "pkill -q dunst"
  spawnOnce "dunst &"
  spawnOnce "pkill -q nextcloud"
  spawnOnce "nextcloud &"
  spawnOnce "pkill -q nm-applet"
  spawnOnce "nm-applet &"
  spawnOnce "pkill -q flameshot"
  spawnOnce "flameshot &"
  spawnOnce "pkill -q unclutter"
  spawnOnce "unclutter --idle 2 &"
  -- spawnOnce "ibus-daemon -drx"
  spawnOnce "xss-lock slock &"
  spawnOnce "$HOME/.config/polybar/launch.sh &"
  spawnOnce "pkill -q signal-desktop"
  spawnOnce "signal-desktop --start-in-tray &"

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main = do
  -- xmproc <- spawnPipe "xmobar /home/samuelsung/.config/xmobar/xmobar.config"
  -- xmproc <- spawnPipe "/home/samuelsung/.config/polybar/launch.sh"
  dbus <- D.connectSession
    -- Request access to the DBus name
  D.requestName dbus (D.busName_ "org.xmonad.Log")
    [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]

  xmonad $ ewmh $ docks def {
    -- simple stuff
    terminal           = myTerminal,
    focusFollowsMouse  = myFocusFollowsMouse,
    clickJustFocuses   = myClickJustFocuses,
    borderWidth        = myBorderWidth,
    modMask            = myModMask,
    workspaces         = myWorkspaces,
    normalBorderColor  = myNormalBorderColor,
    focusedBorderColor = myFocusedBorderColor,

    -- key bindings
    keys               = myKeys,
    mouseBindings      = myMouseBindings,

    -- hooks, layouts
    layoutHook         = myLayout,
    manageHook         = composeAll [
      -- fullscreenManageHook,
      insertPosition Master Newer,
      -- (isFullscreen --> doFullFloat),
      myManageHook],
    handleEventHook    = myEventHook, -- <+> fullscreenEventHook,
    logHook            = dynamicLogWithPP (myLogHook dbus),
    startupHook        = myStartupHook
  }
