{ pkgs, ... }:

{
  home.packages = with pkgs; [
    (writeShellScriptBin "samedir" ./samedir)
    (writeShellScriptBin "dmenu_emoji" ./dmenu_emoji)
    (writeShellScriptBin "youtube-dl-mpv" ''${builtins.readFile ./youtube-dl-mpv}'')
  ];
}
