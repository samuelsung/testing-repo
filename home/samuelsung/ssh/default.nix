{ ... }:

{
  programs.ssh = {
    enable = true;
    matchBlocks = {
      "gitlab.samuelsung1998.net" = {
        checkHostIP = true;
        port = 939;
        identityFile = "~/.ssh/samuelsung1998.net";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "linode-general" = {
        checkHostIP = true;
        port = 939;
        identityFile = "~/.ssh/samuelsung1998.net";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "tr3960x" = {
        checkHostIP = true;
        port = 939;
        identityFile = "~/.ssh/samuelsung1998.net";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "xps-15-9500" = {
        checkHostIP = true;
        port = 939;
        identityFile = "~/.ssh/samuelsung1998.net";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "jellyfin" = {
        checkHostIP = true;
        port = 939;
        identityFile = "~/.ssh/samuelsung1998.net";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "reverseproxy" = {
        checkHostIP = true;
        port = 939;
        identityFile = "~/.ssh/samuelsung1998.net";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "bitwarden" = {
        checkHostIP = true;
        port = 939;
        identityFile = "~/.ssh/samuelsung1998.net";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "local-gitlab-runner" = {
        checkHostIP = true;
        port = 939;
        identityFile = "~/.ssh/samuelsung1998.net";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "scm-server.ifshk.com" = {
        checkHostIP = true;
        identityFile = "~/.ssh/garemarudo-scm";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "garemarudo" = {
        checkHostIP = true;
        port = 939;
        identityFile = "~/.ssh/garemarudo";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "gitlab.com" = {
        checkHostIP = true;
        identityFile = "~/.ssh/gitlab.com";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
      "altssh.gitlab.com" = {
        checkHostIP = true;
        port = 443;
        user = "git";
        identityFile = "~/.ssh/gitlab.com";
        extraOptions = {
          Preferredauthentications = "publickey";
        };
      };
    };
  };
}
