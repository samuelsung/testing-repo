{ config, pkgs, ... }:

{
  home.packages = with pkgs; [ dmenu dwm st ];
}
