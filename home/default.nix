{ self
, home-manager
, nixpkgs-alt-overlay
, nur
, custom-package-overlay
}:

let
  inherit (home-manager.lib) homeManagerConfiguration;
  samuelsung-config = import ./samuelsung;
  modules = import ../modules;

  hmconfig =
    { system ? "x86_64-linux"
    , aspect-ratio ? "16:9"
    , merge-strategies ? "theirs"
    , workPC ? true
    }:
    let
      overlays = { ... }: {
        nixpkgs.overlays = [
          nixpkgs-alt-overlay
          custom-package-overlay
          nur.overlay
        ];
      };

      common = { ... }: {
        imports = with modules; [
          nix-unfree
          overlays
        ];
      };

      gui-common-config = { workUser ? true }: { ... }: {
        imports = with samuelsung-config; [
          dunst
          fcitx
          (firefox {
            withProxyBlock = workPC || workUser;
            withGames = !workPC && !workUser;
            withSocialMedia = !workPC && !workUser;
            withShopping = !workPC && !workUser;
            withSecretProfile = !workPC && !workUser;
          })
          flameshot
          gtk
          # ibus
          newsboat
          picom
          polybar
          ranger
          suckless
          # sxhkd
          vscodium
          xmonad
          (xorg { })
        ];

      };

      cli-common-config = { ... }: {
        imports = with samuelsung-config; [
          git
          neofetch
          # neovim
          neovim-lua
          nix-direnv
          scripts
          ssh
          tmux
          vifm
          xdg
          zsh
        ];
      };

      games-config = { ... }: {
        imports = with samuelsung-config; [
          (final-fantasy-xiv {
            inherit aspect-ratio merge-strategies;
          })
          games
        ];
      };
    in
    {
      samuelsung = homeManagerConfiguration {
        inherit system;
        homeDirectory = "/home/samuelsung";
        username = "samuelsung";
        configuration = { ... }: { };

        extraModules = [
          common
          cli-common-config
          (gui-common-config { workUser = false; })
          games-config
        ];
      };

      samuelsung_work = homeManagerConfiguration {
        inherit system;
        homeDirectory = "/home/samuelsung_work";
        username = "samuelsung_work";
        configuration = { ... }: { };

        extraModules = [
          common
          cli-common-config
          (gui-common-config { workUser = true; })
        ];
      };
    };
in
{
  tr3960x = hmconfig {
    workPC = false;
    aspect-ratio = "48:9";
    merge-strategies = "ours";
  };
  xps-15-9500 = hmconfig {
    workPC = false;
  };
  blade-stealth = hmconfig {
    workPC = false;
  };
  garemarudo = hmconfig {
    workPC = true;
  };
}
