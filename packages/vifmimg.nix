{ lib, fetchFromGitHub, stdenv }:

stdenv.mkDerivation rec {
  name = "vifmimg";

  src = fetchFromGitHub {
    owner = "cirala";
    repo = "vifmimg";
    rev = "98c6fbdccb63853c1c79b9e4d9a0905275ae9e12";
    sha256 = "19b6vqxd6y1l8zv7lasd2gzn0n5z74frkkr5fl5gfp5ckd7j69sz";
  };

  dontBuild = true;

  installPhase = ''
    mkdir -p $out/bin

    mv vifmimg $out/bin
    mv vifmrun $out/bin
  '';

  meta = with lib; {
    homepage = "https://github.com/cirala/vifmimg";
    description = "Image previews using Überzug for vifm (vi file manager)";
    license = licenses.gpl3;
    platforms = platforms.linux;
  };
}
