{ appimageTools, stdenv, lib, fetchurl }:
let
  name = "osu";
  version = "2021.515.0";

  src = fetchurl {
    url = "https://github.com/ppy/osu/releases/download/${version}/osu.AppImage";
    sha256 = "14rrs6l8l38na7pq71hnc5gdwsrvrriyzcfljf2sqm9f8d1p1g83";
  };

  unpacked = appimageTools.extractType2 {
    inherit name src;
  };

in appimageTools.wrapType2 {
  inherit name src;

  extraInstallCommands = ''
    install -m 444 -D ${unpacked}/osu!.desktop $out/share/applications/osu!.desktop
    install -m 444 -D ${unpacked}/osu!.png $out/share/applications/osu!.png
  '';

  extraPkgs = pkgs: [ pkgs.icu ];

  meta = with lib; {
    description = "rhythm is just a click away";
    homepage = "https://osu.ppy.sh";
    license = licenses.mit;
    platforms = platforms.linux;
  };

}
