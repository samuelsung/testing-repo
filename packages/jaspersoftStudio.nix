{ pkgs
, stdenv
, fetchurl
}:

# TODO
stdenv.mkDerivation {
  name = "Jaspersoft Studio";
  version = "6.16.0";

  src = fetchurl {
    url = "https://sourceforge.net/projects/jasperstudio/files/JaspersoftStudio-6.16.0/TIB_js-studiocomm_6.16.0_linux_x86_64.tgz";
    sha256 = "1q3nws1fp2qhavckgw0ninyh7l68ypmnrlmh6bshwnbr89062904";
  };

  nativeBuildInputs = with pkgs; [
    unzip
    autoPatchelfHook
  ];

  buildInputs = (with pkgs; [
    fontconfig freetype glib gsettings-desktop-schemas gtk2 gtk3 jdk zlib
  ]) ++ (with pkgs.xorg; [
    libX11 libXrender libXtst
  ]);

  unpackPhase = ''
    unzip $src
  '';

  installPhase = ''
    # install -m755 -D "Jaspersoft Studio" $out/bin/JaspersoftStudio
  '';

  meta = {
    homepage = "http://www.eclipse.org/";
    platforms = [ "x86_64-linux" ];
  };
}
