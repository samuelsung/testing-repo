{ appimageTools, stdenv, lib, fetchurl }:
let
  name = "figma-linux";
  version = "0.8.1";

  src = fetchurl {
    url = "https://github.com/Figma-Linux/figma-linux/releases/download/v${version}/figma-linux_${version}_linux_x86_64.AppImage";
    sha256 = "0r6fasd1gpsqn2qj1qyqicv3y15w51cbl3czznnc6rkw53ps1mvm";
  };

  unpacked = appimageTools.extractType2 {
    inherit name src;
  };

in appimageTools.wrapType2 {
  inherit name src;

  extraInstallCommands = ''
    install -Dm444 ${unpacked}/figma-linux.desktop -t $out/share/applications
    install -Dm444 ${unpacked}/figma-linux.png -t $out/share/applications
    substituteInPlace $out/share/applications/figma-linux.desktop \
        --replace 'Exec=AppRun --no-sandbox' 'Exec=${name}'
  '';

  meta = with lib; {
    description = "electron wrapper for figma";
    homepage = "https://https://github.com/Figma-Linux/figma-linux";
    license = licenses.gpl2;
    platforms = platforms.linux;
  };
}
