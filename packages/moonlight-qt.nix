{ stdenv, lib, fetchFromGitHub, pkgs }:

with pkgs;
stdenv.mkDerivation {
  name = "moonlight-qt";
  version = "2.1.0";

  src = fetchFromGitHub {
    owner = "moonlight-stream";
    repo = "moonlight-qt";
    rev = "81943b12c0fa730a80896c92899329b101013782";
    sha256 = "lYA6moprW5gg/Zf81pZOSECvm5eeQW7ewUiYoj9a5Tg=";
    fetchSubmodules = true;
  };

  nativeBuildInputs = with qt5; [ qmake qttools wrapQtAppsHook ];

  buildInputs = [
    ffmpeg libva libvdpau libopus SDL2 SDL2_ttf libevdev udev libpulseaudio alsaLib pkgconfig
  ] ++ (with qt5; [
    qtbase qtquickcontrols2 qtsvg libpulseaudio qtmultimedia
  ]);

  qmakeFlags = ["moonlight-qt.pro"];

  meta = with lib; {
    homepage = "https://github.com/moonlight-stream/moonlight-qt";
    description = "Moonlight-qt";
    license = licenses.gpl3Plus;
    maintainers = [];
    platforms = platforms.linux;
  };
}
