{ fetchurl, lib, stdenv, pkgs }:

with pkgs;
stdenv.mkDerivation rec {
  name = "figma-linux";
  version = "0.6.4";

  src = fetchurl {
    url = "https://github.com/Figma-Linux/figma-linux/releases/download/v${version}/figma-linux_${version}_amd64.deb";
    sha256 = "1q3nws1fp2qhavckgw0ninyh7l68ypmnrlmh6bshwnbr89w629p4";
  };

  nativeBuildInputs = [ makeWrapper dpkg ];

  unpackPhase = ''
    mkdir pkg
    dpkg-deb -x $src pkg
    sourceRoot=pkg
  '';

  buildInputs = (with xorg; [
    libX11 libxcb libXext libXtst
  ]) ++ [
    systemd sqlite gtk3 gtk2 alsaLib
  ];

  desktopItem = makeDesktopItem {
    inherit name;
    desktopName = "figma-linux";
    exec = "figma-linux %U";
    type = "Application";
    categories = "Graphics";
    mimeType = "application/figma;x-scheme-handler/figma";
  };

  installPhase = let
    libPath = lib.makeLibraryPath ([
      stdenv.cc.cc glib nss nspr gdk-pixbuf gtk3 pango atk at-spi2-atk at-spi2-core cairo dbus expat utillinux alsaLib cups freetype fontconfig libpulseaudio udev gcc-unwrapped
    ] ++ (with xorg; [
      libX11 libxcb libXcomposite libXcursor libXdamage libXext libXfixes libXi libXrender libXrandr libXScrnSaver libXtst
    ]));
  in ''
    mkdir -p "$out/bin"
    mv opt "$out/"

    patchelf \
      --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
      --set-rpath "${libPath}:\$ORIGIN" \
      $out/opt/figma-linux/figma-linux

    makeWrapper $out/opt/figma-linux/figma-linux $out/bin/figma-linux \
      --prefix LD_LIBRARY_PATH : ${lib.makeLibraryPath [ libpulseaudio udev gcc-unwrapped ]}

    mkdir -p "$out/share"
    ln -s "${desktopItem}/share/applications" "$out/share/applications"
  '';

  meta = with lib; {
    homepage = "https://github.com/Figma-Linux/figma-linux";
    description = "Figma";
    license = licenses.gpl2;
    maintainers = [];
    platforms = platforms.linux;
  };
}
