{ pkgs, nodejs, stdenv, lib, ... }:

let
  super = import ./node-composition.nix {
    inherit pkgs nodejs;
    inherit (stdenv.hostPlatform) system;
  };
  self = super // {
    sql-language-server = super.sql-language-server.override {
      buildInputs = [ self.node-pre-gyp self.yarn ];
    };
  };
in self
