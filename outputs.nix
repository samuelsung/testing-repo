{ self
, dmenu
, dwm
, final-fantasy-xiv-sync
, flake-utils
, home-manager
, nixos-hardware
, nixpkgs
, nixpkgs-master
, nixpkgs-unstable
, nur
, slock
, sops-nix
, st
}:

let
  config-packages = p: flake-utils.lib.eachDefaultSystem (
    system: {
      packages = import p {
        inherit system;
        config = { allowUnfree = true; };
      };
    }
  );

  nixpkgs-alt-overlay = self: super: {
    dmenu = dmenu.packages.${super.system}.dmenu;
    dwm = dwm.packages.${super.system}.dwm;
    final-fantasy-xiv-sync = final-fantasy-xiv-sync.packages.${super.system}.final-fantasy-xiv-sync;
    slock = slock.packages.${super.system}.slock;
    st = st.packages.${super.system}.st;
    master = (config-packages nixpkgs-master).packages.${super.system};
    unstable = (config-packages nixpkgs-unstable).packages.${super.system};
  };

  # TODO: Move to a separate repository
  custom-package-overlay = self: super: {
    custom.figma = self.callPackage ./packages/figma-appimage.nix { };

    custom.jdtls = self.callPackage ./packages/jdtls.nix { };

    custom.moonlight-qt = self.callPackage ./packages/moonlight-qt.nix { };

    custom.nodePackages = self.callPackage ./packages/nodePackages { };

    custom.osu = self.callPackage ./packages/osu.nix { };

    custom.vifmimg = self.callPackage ./packages/vifmimg.nix { };

    custom.vimPlugins = self.callPackage ./packages/vimPlugins.nix {
      inherit (self.vimUtils)
        buildVimPlugin
        buildVimPluginFrom2Nix;
    };

    custom.vscode-extensions = self.callPackage ./packages/vscodeExtensions.nix {
      inherit (self.vscode-utils)
        extensionFromVscodeMarketplace;
    };

    custom.python3Packages = self.callPackage ./packages/python3Packages {
      inherit (self.python3Packages)
        buildPythonPackage
        setuptools;
    };
  };

in

(flake-utils.lib.eachDefaultSystem (
  system:
  let
    pkgs = nixpkgs.legacyPackages.${system};
  in
  {
    apps.hm-build = {
      type = "app";
      program = toString (pkgs.writeScript "hm-build" ''
        #!${pkgs.runtimeShell}
        set -eu -o pipefail
        export PATH=${pkgs.lib.makeBinPath [ pkgs.git pkgs.coreutils pkgs.nixFlakes pkgs.jq ]}
        machine=$HOSTNAME
        user=$(whoami)
        flake=$(nix flake metadata --json ${./.} | jq -r .url)
        set -x
        nix build --no-link --show-trace --json ".#hmConfigurations.''${machine}.''${user}.activationPackage" "$@" | jq -r '.[] | .outputs | .out'
      '');
    };
    apps.hm-switch = {
      type = "app";
      program = toString (pkgs.writeScript "hm-switch" ''
        #!${pkgs.runtimeShell}
        set -eu -o pipefail -x
        cd ${./.}
        $(nix run .#hm-build -- "$@")/activate
      '');
    };

    apps.updatekeys = {
      type = "app";
      program = toString (pkgs.writeScript "updatekeys" ''
        #!${pkgs.runtimeShell}

        ${pkgs.gnused}/bin/sed '/path_regex/!d;s/.*path_regex: //g' .sops.yaml | while read -r p; do {
          ${pkgs.findutils}/bin/find . | ${pkgs.gnugrep}/bin/grep "$p" | while read -r f; do {
            ${pkgs.sops}/bin/sops updatekeys -y "$f"
          }; done;
        }; done;
      '');
    };
  }
)) // {
  nixosConfigurations = import ./machines {
    inherit (nixpkgs.lib) nixosSystem;
    inherit
      nixos-hardware
      nixpkgs
      nixpkgs-master
      nixpkgs-unstable
      nixpkgs-alt-overlay
      nur
      sops-nix
      custom-package-overlay;
  };

  hmConfigurations = import ./home {
    inherit
      self
      home-manager
      nixpkgs-alt-overlay
      nur
      custom-package-overlay;
  };
}
